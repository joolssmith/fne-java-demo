package com.flexera.demo.fne;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.flexera.demo.fne.client.Client;
import com.flexera.demo.fne.client.DesiredFeature;
import com.flexera.demo.fne.client.Rights;

public class Main {

  static Log log = LogFactory.getLog(Main.class);

  public static void main(final String... args) {

    Main.log.info(String.format("starting in %s", System.getProperty("user.dir")));
    try {
      final Client client = new Client() {
        {
          // set the hostid
          this.setHostid("jools-test-2018-08-09");
          // identity data - can be created from toolkit tools or directly from FNO which
          // is better
          this.setIdentity(IdentityClient.IDENTITY_DATA);
          // trusted storage location - can be NULL which uses volatile TS
          this.setStoragePath(Paths.get("d:", "git", "fne-java-demo", "storage"));
          this.setAlias("Jools Test");
          // device model - normally just use the defaults
          this.setModel(Client.StandardClientModel);
          // FNO, CLS or LLS URL
          this.setUrl(Client.generateCloudLicenseServerUrl("flex1113-uat", "PG7U70UUYWJ2"));
        }
      };

      final boolean borrowing = true;
      
      client.initialize();

      client.showAvailableHostids();

      client.showFeaturesInTrustedStorage();

      final List<DesiredFeature> features = Arrays
          .asList(new DesiredFeature[] { 
            new DesiredFeature("Cienna-A", "1.0", 1),
            new DesiredFeature("Cienna-C", "1.0", 1) 
        });
      
      final List<Rights> rightsIds = Arrays.asList(
          new Rights[] { 
              new Rights("dde1-f03c-d1a2-4159-8b55-276a-04e5-e837", 1) });
      
      // licenses typically activated from FNO or borrowed from a license server
      // and placed in trusted storage
      if (borrowing) {
        // borrow
        client.borrow(features);
      }
      else {
        // activate 
        client.activate(rightsIds);
      }
      
      // call home
      // client.activate();

      // acquire licenses
      for (final DesiredFeature f : features) {
        client.acquire(f);
      }

      // release licenses
      for (final DesiredFeature f : features) {
        client.release(f);
      }

      client.terminate();
    }
    catch (final Throwable t) {
      Main.log.fatal(t);
    }
    finally {
      Main.log.info("quitting");
    }
  }
}
