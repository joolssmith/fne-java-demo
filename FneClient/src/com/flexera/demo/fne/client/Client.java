package com.flexera.demo.fne.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.flexnet.licensing.client.ICapabilityRequestOptions;
import com.flexnet.licensing.client.ICapabilityResponseData;
import com.flexnet.licensing.client.IDesiredFeatureOptions;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.SharedConstants.ExpirationUnits;
import com.flexnet.lm.SharedConstants.ExpirationValidation;
import com.flexnet.lm.SharedConstants.RequestOperation;
import com.flexnet.lm.net.Comm;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * 
 * @author juliansmith
 *
 */
public class  Client {
  private byte[] Identity;
  private String Hostid;
  private String Model;
  private String Alias;
  private Path StoragePath;
  private URL Url;

  private ILicensing licensing;
  private ILicenseManager licenseManager;

  private List<ILicense> acquiredFeatures = new ArrayList<>();
  
  public static String StandardClientModel = "FLX_CLIENT";
  public static String StandardServerModel = "FLX_SERVER";

  final ObjectMapper json = new ObjectMapper();

  protected Client() {
    this.json.configure(SerializationFeature.INDENT_OUTPUT, true);
  }
  
  public static URL generateCloudActivationUrl(final String tenant) throws MalformedURLException {
    return new URL("https", 
        String.format("%s.compliance.flexnetoperations.com", tenant), 
        443, 
        "/deviceservices");
  }

  public static URL generateOnPremisesActivationUrl(final int port) throws MalformedURLException {
    return new URL("http", "localhost", port, "/flexnet/deviceservices");
  }

  public static URL generateOnPremisesActivationUrl() throws MalformedURLException {
    return Client.generateOnPremisesActivationUrl(8888);
  }

  public static URL generateCloudLicenseServerUrl(final String tenant, final String instance)
      throws MalformedURLException {
    return new URL("https", 
        String.format("%s.compliance.flexnetoperations.com", tenant), 
        443,
        String.format("/instances/%s/request", instance));
  }

  public static URL generateLocalLicenseServerUrl(final int port) throws MalformedURLException {
    return new URL("http", "localhost", port, "/fne/bin/capability");
  }

  public static URL generateLocalLicenseServerUrl() throws MalformedURLException {
    return Client.generateLocalLicenseServerUrl(7070);
  }

  static Log log = LogFactory.getLog(Client.class);

  /**
   * Rely on exceptions to indicate failure
   *
   * @throws FlxException
   */
  public void initialize() throws FlxException {

    // note alias just used for convenient naming
    Client.log.info("initializing licensing");
    this.licensing = LicensingFactory.getLicensing(this.Identity, this.StoragePath.toString(), this.Hostid, this.Alias);

    Client.log.info("initializing license manager");
    this.licenseManager = this.licensing.getLicenseManager();

    this.licenseManager.setHostName(this.Alias);

    this.licenseManager.setHostType(this.Model);
  }

  /**
   * 
   */
  public void terminate() {
    throw new NotImplementedException();
  }

  /**
   * 
   * @throws FlxException
   * @throws IOException
   */
  public void activate() throws FlxException, IOException {
    activate(new ArrayList<Rights>());
  }
  
  /**
   * 
   * @throws FlxException
   * @throws JsonProcessingException
   */
  public void showAvailableHostids() throws FlxException, JsonProcessingException {
    log.debug("avilble (native) hostids on this device");
    log.debug("hostids: " + this.json.writeValueAsString(this.licenseManager.getHostIds()));
  }
  
  /**
   * 
   * @throws FlxException
   * @throws JsonProcessingException
   */
  public void showFeaturesInTrustedStorage() throws FlxException, JsonProcessingException {
    log.debug("features in trusted storage");
    final List<IFeature> features = this.licenseManager.getFeaturesFromTrustedStorage(true);
    log.debug("features: " + this.json.writeValueAsString(features));
  }
  
  /**
   * 
   * @param rights
   * @throws FlxException
   * @throws IOException
   */
  public void activate(final Collection<Rights> rights) throws FlxException, IOException {

    log.debug("creating cpability request options");
    final ICapabilityRequestOptions options = this.licenseManager.createCapabilityRequestOptions(); 
    
    log.debug("force reseponse");
    options.forceResponse();
    
    log.debug("adding rights ids");
    rights.forEach(right -> {
      log.debug(right.toString());
      options.addRightsId(right.getActivationId(), right.getCount());
    });
    options.setIncremental(false);
    options.setRequestOperation(RequestOperation.REQUEST);
    
    log.debug("generate binary response");
    final byte[] requestData = this.licenseManager.generateCapabilityRequest(options);
    Files.write(Paths.get("request.bin"), requestData);
    
    log.debug("communicate with server - " + this.Url.toString());
    final Comm conn = Comm.getHttpInstance(this.Url.toString());
    
    final byte[] responseData = conn.sendBinaryMessage(requestData);
    Files.write(Paths.get("response.bin"), responseData);
    
    log.debug("process response");
    final ICapabilityResponseData response = licenseManager.processCapabilityResponse(responseData);
    
    log.debug("response: " + this.json.writeValueAsString(response));
  }

  /**
   * 
   * @param features
   * @throws FlxException 
   * @throws IOException 
   */
  public void borrow(final Collection<DesiredFeature> features) throws FlxException, IOException {
    log.debug("creating cpability request options");
    final ICapabilityRequestOptions options = this.licenseManager.createCapabilityRequestOptions(); 
    options.setIncremental(false);
    options.setRequestOperation(RequestOperation.REQUEST);
    options.forceResponse();
    options.setBorrowInterval(10);
    options.setBorrowUnits(ExpirationUnits.MINUTE);

    final IDesiredFeatureOptions featureOpts = this.licenseManager.createDesiredFeatureOptions();
    featureOpts.setPartial(false);

    log.debug("adding desired features");
    features.forEach(feature -> {
      log.debug(feature.toString());
      options.addDesiredFeature(feature.getName(), feature.getVersion(), feature.getCount(), featureOpts);
    });

    log.debug("generate binary response");
    final byte[] requestData = this.licenseManager.generateCapabilityRequest(options);
    Files.write(Paths.get("request.bin"), requestData);
    
    log.debug("communicate with server - " + this.Url.toString());
    final Comm conn = Comm.getHttpInstance(this.Url.toString());
    
    final byte[] responseData = conn.sendBinaryMessage(requestData);
    Files.write(Paths.get("response.bin"), responseData);
    
    log.debug("process response");
    final ICapabilityResponseData response = licenseManager.processCapabilityResponse(responseData);
    
    log.debug("response: " + this.json.writeValueAsString(response));
  }

  /**
   * 
   * @param feature
   * @throws FlxException
   * @throws JsonProcessingException
   */
  public void acquire(final DesiredFeature feature) throws FlxException, JsonProcessingException {
    
    log.debug("acquire " + feature.toString());
    
    final ILicense license = this.licenseManager.acquire(
        feature.getName(),
        feature.getVersion(),
        feature.getCount());
    
    //keep local copy
    this.acquiredFeatures.add(license);
    
    log.debug("response: " + this.json.writeValueAsString(license));
  }

  /**
   * 
   * @param feature
   * @throws FlxException
   * @throws JsonProcessingException
   */
  public void release(final DesiredFeature feature) throws FlxException, JsonProcessingException {
    
    log.debug("release " + feature.toString());
    final Optional<ILicense> resp = this.acquiredFeatures.stream()
        .filter(license -> feature.matches(license))
        .findFirst();
    
    if (resp.isPresent()) {
      final ILicense lic = resp.get();

      this.acquiredFeatures.remove(lic);
      
      log.debug("releasing: " + this.json.writeValueAsString(lic));
      this.licenseManager.returnLicense(lic);
    }
  }

  // getters and setters
  
  public byte[] getIdentity() {
    return Identity;
  }

  public void setIdentity(final byte[] identity) {
    Identity = identity;
  }

  public String getHostid() {
    return Hostid;
  }

  public void setHostid(final String hostid) {
    Hostid = hostid;
  }

  public String getModel() {
    return Model;
  }

  public void setModel(final String model) {
    Model = model;
  }

  public String getAlias() {
    return Alias;
  }

  public void setAlias(final String alias) {
    Alias = alias;
  }

  public Path getStoragePath() {
    return StoragePath;
  }

  public void setStoragePath(final Path storagePath) {
    StoragePath = storagePath;
  }

  public URL getUrl() {
    return Url;
  }

  public void setUrl(URL url) {
    Url = url;
  }
  
  // getters setters
  
  
}
