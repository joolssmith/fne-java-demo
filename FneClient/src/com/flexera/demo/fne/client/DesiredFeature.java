package com.flexera.demo.fne.client;

import com.flexnet.licensing.client.ILicense;

/**
 * @author juliansmith
 *
 */
public class DesiredFeature {
  private String Name;
  private String Version;
  private int Count;

  public DesiredFeature(final String name, final String version, final int count) {
    this.Name = name;
    this.Version = version;
    this.Count = count;
  }
  // getters

  public String getName() {
    return this.Name;
  }

  public String getVersion() {
    return this.Version;
  }

  public int getCount() {
    return this.Count;
  }

  @Override
  public String toString() {
    return "DesiredFeature [Name=" + this.Name + ", Version=" + this.Version + ", Count=" + this.Count + "]";
  }

  public boolean matches(final ILicense license) {
    return this.Name.equals(license.getName()) 
        && this.Version.equals(license.getVersion())
        && this.Count == license.getCount();
  }
}
