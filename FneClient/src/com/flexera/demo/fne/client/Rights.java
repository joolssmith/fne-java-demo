package com.flexera.demo.fne.client;

public class Rights {
  private String ActivationId;
  private int Count;

  public Rights(final String activationId, final int count) {
    this.ActivationId = activationId;
    this.Count = count;
  }
  // getters

  public String getActivationId() {
    return this.ActivationId;
  }

  public int getCount() {
    return this.Count;
  }

  @Override
  public String toString() {
    return "Rights [ActivationId=" + this.ActivationId + ", Count=" + this.Count + "]";
  }

  public void setActivationId(final String activationId) {
    this.ActivationId = activationId;
  }

  public void setCount(final int count) {
    this.Count = count;
  }

}
