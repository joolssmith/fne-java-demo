@echo off
set "CMD_LINE_ARGS= "
:setupArgs
if "%~1" == "" goto :doneArgs
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift /1
goto :setupArgs
:doneArgs

:: figure out where things are

set SCRIPT_DIR=%~dp0
set LIB=%SCRIPT_DIR%\..\..\lib
set CLASSES=%SCRIPT_DIR%\classes

:: find Java

set JAVA=%JAVA_HOME%\bin\java.exe
:: Use JAVA_HOME?
if exist "%JAVA%" goto :javaOK
:: Just find java in the path
set JAVA=java
:javaOK

"%JAVA%" -cp "%CLASSES%;%LIB%\flxTools.jar;%LIB%\flxBinary.jar;%LIB%\EccpressoAll.jar;%LIB%\commons-codec-1.9.jar" com.flexnet.lm.tools.CapabilityResponseUtil %CMD_LINE_ARGS%
