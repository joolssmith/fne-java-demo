@echo off
set "CMD_LINE_ARGS= "
:setupArgs
if "%~1" == "" goto :doneArgs
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift /1
goto :setupArgs
:doneArgs

:: figure out where things are

set SCRIPT_DIR=%~dp0
set LIB=%SCRIPT_DIR%\..\..\lib
set ULLS_LIB=%SCRIPT_DIR%\..\lib
set CLASSES=%SCRIPT_DIR%\classes

:: find Java

set JAVA=%JAVA_HOME%\bin\java.exe
:: Use JAVA_HOME?
if exist "%JAVA%" goto :javaOK
:: Just find java in the path
set JAVA=java
:javaOK

"%JAVA%" -cp "%CLASSES%;%ULLS_LIB%\flxPublicTools.jar;%LIB%\flxPublicTools.jar;%ULLS_LIB%\EccpressoAll.jar;%LIB%\EccpressoAll.jar;%ULLS_LIB%\commons-codec-1.9.jar;%LIB%\commons-codec-1.9.jar;%LIB%\jackson-core-2.2.3.jar;%LIB%\jackson-databind-2.2.3.jar;%LIB%\jackson-annotations-2.2.3.jar" com.flexnet.lm.tools.OfflineSync %CMD_LINE_ARGS%
