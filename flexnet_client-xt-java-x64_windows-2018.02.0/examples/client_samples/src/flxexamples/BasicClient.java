/**
 * Copyright (c) 2011-2018 Flexera Software LLC.
 * All Rights Reserved.
 * This software has been provided pursuant to a License Agreement
 * containing restrictions on its use.  This software contains
 * valuable trade secrets and proprietary information of
 * Flexera Software LLC and is protected by law.
 * It may not be copied or distributed in any form or medium, disclosed
 * to third parties, reverse engineered or used in any manner not
 * provided for in said License Agreement except with the prior
 * written authorization from Flexera Software LLC.
 */

package flxexamples;

import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;
import java.nio.file.NoSuchFileException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BasicClient {

	//	
	//	This example program illustrates the general sequence of functions
	//	required to acquire "survey" and "highres" features from a buffer
	//	license source read from a file.
	//
	//	For more comprehensive examples that illustrate additional license sources,
	//	cleanup during error conditions, etc., see the "Client" and other samples
	//	provided in the "examples/client_samples/java" directory of the FlexNet Embedded toolkit.
	//
	private static String licenseFile = "license.bin";
	
	public static void main(String[] args) {
	
		// Validate command-line arguments
		if(!validateArgs(args)) {
			return;
		}
		
		if(IdentityClient.IDENTITY_DATA == null) {
			System.out.println("License-enabled code requires client identity data, " +
								"which you create with pubidutil and printbin -java. " +
								"See the User Guide for more information.");
			return;
		}

		// Initialize ILicensing interface with identity data using file-based trusted storage in
		// user's home directory and hard-coded string hostid "1234567890" inside
		// a try-with-resources block
		try (ILicensing licensing = LicensingFactory.getLicensing(IdentityClient.IDENTITY_DATA, System.getProperty("user.home"), "1234567890", "basicclient")) {
			// Get ILicenseManager interface, the primary object for licensing-related functionality
			ILicenseManager licenseManager = licensing.getLicenseManager();
			// Add buffer license source
			System.out.println("Reading data from " + licenseFile + ".");
			licenseManager.addBufferLicenseSource(Files.readAllBytes(Paths.get(licenseFile)));
			
			// Acquire 1 "survey" license
			acquireLicense(licenseManager, "survey", "1.0", 1);
			
			// Acquire 1 "highres" license
			acquireLicense(licenseManager, "highres", "1.0", 1);
		}
		catch (FlxException e) {
			System.out.println("Licensing exception: " + e.getMessage());
		}
		catch (UnsatisfiedLinkError e) {
			// Make sure the native FlxCore library is available
			System.out.println("Failed to load FlxCore library. Ensure it's in PATH (Windows) or LD_LIBRARY_PATH (other platforms).");
		}
		catch (NoSuchFileException e) {
			System.out.println("No such file: " + e.getMessage());
		}
		catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
	}
	
	private static void acquireLicense(ILicenseManager licenseManager, String name, String version, int count) {
		ILicense	license = null;
		try {
			license = licenseManager.acquire(name,  version, count);
			System.out.println("Successfully acquired \"" + license.getName() + "\", version " + 
					license.getVersion() +  ", " + license.getCount() + " count.");
			try {
				// return license
				licenseManager.returnLicense(license);
			}
			catch (FlxException e) {
				System.out.println("Unable to return " + name + ": " + e.getMessage());
			}
		}
		catch (FlxException e) {
			System.out.println("Unable to acquire " + name + ": " + e.getMessage());
		}
	}	
	
	private static void usage() {
		System.out.println(
						"\nBasicClient [binary_license_file]" +
						"\nAttempts to acquire 'survey' and 'highres' features from a signed " +
						"\nbinary license file." +
						"\n\nIf unset, default binary_license_file is license.bin.");		
	}
	
	private static boolean validateArgs(String[] args) {
		if(args.length > 1) {
			usage();
			return false;
		}
		else if (args.length == 1) {
			if(args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("-help")) {
				usage();
				return false;
			}
			else {
				licenseFile = args[0];
			}
		}
		else {
			System.out.println("Using default license file " + licenseFile + ".");
		}
		return true;
	}
	
}
