/**
 * Copyright (c) 2013-2018 Flexera Software LLC.
 * All Rights Reserved.
 * This software has been provided pursuant to a License Agreement
 * containing restrictions on its use.  This software contains
 * valuable trade secrets and proprietary information of
 * Flexera Software LLC and is protected by law.
 * It may not be copied or distributed in any form or medium, disclosed
 * to third parties, reverse engineered or used in any manner not
 * provided for in said License Agreement except with the prior
 * written authorization from Flexera Software LLC.
 */

package flxexamples;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.ICapabilityRequestOptions;
import com.flexnet.licensing.client.ICapabilityResponseData;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.IResponseStatus;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.SharedConstants.MachineType;
import com.flexnet.lm.SharedConstants.MessageType;
import com.flexnet.lm.SharedConstants.PropStatusCode;
import com.flexnet.lm.net.Comm;
import com.flexnet.lm.resources.Resources;

public class ServedBufferRequest {
	
	private static boolean generateRequest;
	private static boolean storeRequest;
	private static boolean processResponse;
	private static boolean server;
	private static boolean readFromFile;
	private static boolean savefile;
	private static boolean reportStale;

	//	This example program enables you to:
	//	1. Send a capability request for a served buffer over HTTP to the server, and then store and process the response.
	//	2. Write a capability request for a served buffer to a file. This request can be fed into a server
	//	   to generate a response.
	//	3. Read a served buffer response from a file and process accordingly.
	//	4. Manage stale served buffer response(s).

	public static void main(String[] args) {

		// Validate command-line arguments
		if (!validateArgs(args)) {
			return;
		}
		
		if (IdentityClient.IDENTITY_DATA == null) {
			System.out.println("License-enabled code requires client identity data, " +
								"which you create with pubidutil and printbin -java. " +
								"See the User Guide for more information.");
			return;
		}

		// Initialize ILicensing interface with identity data using file-based trusted storage in
		// user's home directory and hard-coded string hostid "1234567890" inside
		// a try-with-resources block
		try (ILicensing licensing = LicensingFactory.getLicensing(IdentityClient.IDENTITY_DATA, System.getProperty("user.home"), "1234567890", "servedbufferrequest")) {
			// Get ILicenseManager interface, the primary object for licensing-related functionality
			ILicenseManager licenseManager = licensing.getLicenseManager();
            // The optional host name is typically set by a user as a friendly name for the host.  
            // The host name is not used for license enforcement.			
			licenseManager.setHostName("Sample Device");
			// The host type is typically a name set by the implementer, and is not modifiable by the user.
			// While optional, the host type may be used in certain scenarios by some back-office systems such as FlexNet Operations.
			licenseManager.setHostType("FLX_CLIENT");
			
			if (reportStale) {
				reportStaleServedBuffers(licenseManager, args[1], args[2]);
				return;
			}
			
			byte[]			requestData = null;
			byte[]			responseData = null;
			
			// Generate a capability request
			if (generateRequest) {
				// Get request options
				ICapabilityRequestOptions	options = licenseManager.createCapabilityRequestOptions();
				// Force response
				options.forceResponse();
				// Served buffer
				options.setServedBuffer();
				// Optional vendor dictionary
				options.addVendorDictionaryItem("key1", "one");
				options.addVendorDictionaryItem("key2", 2);
				// Add any additional options here, such as desired features or rights IDs

				// Generate request
				requestData = licenseManager.generateCapabilityRequest(options);
				if (storeRequest) {
					Files.write(Paths.get(args[1]), requestData, StandardOpenOption.CREATE_NEW);
					System.out.println("Successfully generated request to " + args[1] + ".");
				}
			}
			// Communicate with server
			if (server) {
				responseData = talkToServer(requestData, args[1]);
			}

			// Read response from file
			if (readFromFile) {
				System.out.println("Reading served buffer response from " + args[1] + ".");
				responseData = Files.readAllBytes(Paths.get(args[1]));
				if (responseData.length == 0) {
					System.out.println("Error: " + args[1] + " contains no data.");
					return;
				}
			}

			if (processResponse && (responseData != null) && (responseData.length > 0)) {
				// Process response, for served buffer addServedBufferLicenseSource can also be used
				ICapabilityResponseData responseDetails = licenseManager.addServedBufferLicenseSource(responseData);
				System.out.println("Successfully processed response.");
				if(responseDetails != null) {
					// Get response details
					getResponseDetails(responseDetails);
				}
	            // Features in processed response
	            List<IFeature> features = responseDetails.getFeatures();
	            System.out.println("Number of features loaded from served buffer response: " + features.size());

				// Acquire 1 "survey" license
				acquireLicense(licenseManager, "survey", "1.0", 1);
			}
			
			// store response
			if (savefile && (responseData != null) && (responseData.length > 0) ) {
				Files.write(Paths.get(args[3]), responseData, StandardOpenOption.CREATE_NEW);
				System.out.println("Successfully stored response to " + args[3] + ".");
			}
		}
		catch (FlxException e) {
			System.out.println("Licensing exception: " + e.getDiagnosticMessage());
		}
		catch (UnsatisfiedLinkError e) {
			// Make sure the native FlxCore library is available
			System.out.println("Failed to load FlxCore library. Ensure it's in PATH (Windows) or LD_LIBRARY_PATH (other platforms).");
		}
		catch (NoSuchFileException e) {
			System.out.println("No such file: " + e.getMessage());
		}
		catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
	}
	
	private static void getResponseDetails(ICapabilityResponseData responseDetails) {
		try {
			System.out.println("Response data");

			// Vendor dictionary
			System.out.println("Vendor dictionary:");
			Map<String, Object>	vendorDictionary = responseDetails.getVendorDictionary();
			if (vendorDictionary.size() > 0) {
				Set<String> keys = vendorDictionary.keySet();
				for(String key : keys) {
					Object	value = vendorDictionary.get(key);
					System.out.println("\t" + key + "=" + value.toString());
				}
			}
			else {
				System.out.println("\tnone");
			}

			// Status code
			System.out.println("Response status:");
			List<IResponseStatus> statusCodes = responseDetails.getResponseStatus();
			if(statusCodes.size() > 0) {
				for(IResponseStatus status : statusCodes) {
					PropStatusCode code = PropStatusCode.findId(status.getCode());
					String description = Resources.getString(code.getKey());
					System.out.println("\t" + "category: " + status.getCategory().toString().toLowerCase() + 
							", code: " + status.getCode() + (description == null ? "" : " (" + description + ")") +
							", details: " + status.getDetails());
				}
			}
			else {
				System.out.println("\tnone");
			}

			// Virtual machine info
			System.out.println("Machine Type:");
			MachineType	machineType = responseDetails.getVirtualMachineType();
			if (machineType == MachineType.VIRTUAL) {
				System.out.println("\tVIRTUAL");
				Map<String, Object> vmInfo = responseDetails.getVirtualMachineInfo();
				if (vmInfo.size() > 0) {
					Set<String> keys = vmInfo.keySet();
					for (String key : keys) {
						Object value = vmInfo.get(key);
						System.out.println("\t\t" + key + "=" + value.toString());
					}
				}
			}
			else if (machineType == MachineType.PHYSICAL) {
				System.out.println("\tPHYSICAL");
			}
			else {
				System.out.println("\tUNKNOWN");
			}

		}
		catch (FlxException e) {
			System.out.println("Error getting response details.");
		}
	}


	private static byte[] talkToServer(byte[] request, String uri) {
	    byte[] response = null;
	    try {
	        Comm conn = Comm.getHttpInstance(uri);
	        response = conn.sendBinaryMessage(request);
	    }
	    catch (FlxException e) {
	        e.printStackTrace();
	    }
	    return response;
	}

	private static void acquireLicense(ILicenseManager licenseManager, String name, String version, int count) {
		ILicense	license = null;
		try {
			license = licenseManager.acquire(name,  version, count);
			System.out.println("Successfully acquired \"" + license.getName() + "\", version " +
					license.getVersion() +  ", " + license.getCount() + " count.");
			try {
				// return license
				licenseManager.returnLicense(license);
			}
			catch (FlxException e) {
				System.out.println("Unable to return " + name + ": " + e.getMessage());
			}
		}
		catch (FlxException e) {
			System.out.println("Unable to acquire " + name + ": " + e.getMessage());
		}
	}
	
	
	/**
	 * Report stale served buffer responses from the specified directory.
	 * @param licenseManager	LicenseManager
	 * @param filename1			First file
	 * @param filename2			Second file			
	 */
	private static void reportStaleServedBuffers(ILicenseManager licenseManager, String filename1, String filename2) {
		try {
			if (filename1.equals(filename2)) {
				System.out.println("-reportstale option file names cannot be the same.");
				return;
			}
			
			byte[] buffer1 = Files.readAllBytes(Paths.get(filename1));
			if (MessageType.BUFFER_CAPABILITY_RESPONSE != licenseManager.getMessageType(buffer1)) {
				System.out.println(filename1 + " is not a served buffer response file.");
				return;
			}
			ICapabilityResponseData responseData1 = licenseManager.getResponseDetails(buffer1);

			byte[] buffer2 = Files.readAllBytes(Paths.get(filename2));
			if (MessageType.BUFFER_CAPABILITY_RESPONSE != licenseManager.getMessageType(buffer2)) {
				System.out.println(filename2 + " is not a served buffer response file.");
				return;
			}
			ICapabilityResponseData responseData2 = licenseManager.getResponseDetails(buffer2);

			// confirm same server hostid
			if (!responseData1.getServerId().toString().equals(responseData2.getServerId().toString())) {
				System.out.println("Files are from different servers.");
				return;
			}
			
			// compare stale
			System.out.println("Stale file: " + ( responseData1.getServedTime().after(responseData2.getServedTime()) ? filename2 : filename1) );
		}
		catch (FlxException e) {
			System.out.println("Unable to report stale served buffers: " + e.getMessage());
		}
		catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
	}
	
	
	private static void usage() {
		System.out.println(
				"\nServedBufferRequest [-generate outputfile]" +
				"\nServedBufferRequest [-reportstale inputfile inputfile1]" +
				"\nServedBufferRequest [-process inputfile]" +
				"\nServedBufferRequest [-server url [-save outputfile]]\n" +
				"\nwhere:" +
				"\n-generate    Generate capability request into a file." +
		        "\n-reportstale If 'inputfile' and 'inputfile1' originate from the same" +
				"\n             server a determination will be made as to which file is older." +
		        "\n-process     Processes served buffer response from a file." +
				"\n-server      Sends request to a server and processes the response." +
		        "\n             For the test back-office server, http://hostname:8080/request." +
		        "\n             For FlexNet Operations, use http://hostname:8888/flexnet/deviceservices." +
		        "\n             For FlexNet Embedded License Server, use http://hostname:7070/fne/bin/capability." +
		        "\n             For Cloud License Server, use https://<tenant>.compliance.flexnetoperations.com/instances/<instance-id>/request." +
		        "\n             For FNO Cloud, use https://<tenant>.compliance.flexnetoperations.com/deviceservices." +
		        "\n-save        Saved buffer response file name.");
	}

	private static boolean validateArgs(String[] args) {
		if (args.length > 0) {
			if ( args[0].equalsIgnoreCase("-generate")) {
				if (args.length == 2) {
					generateRequest = true;
					storeRequest = true;
					return true;
				}
			}
			else if ( args[0].equalsIgnoreCase("-process")) {
				if (args.length == 2) {
					processResponse = true;
					readFromFile = true;
					return true;
				}
			}
			else if ( args[0].equalsIgnoreCase("-server")) {
				if (args.length == 2 || args.length == 4) {
					generateRequest = true;
					processResponse = true;
					server = true;
					if (args.length == 4) {
						if (args[2].equalsIgnoreCase("-save")) {
							savefile = true;
							return true;
						}
					}
					else {
						return true;
					}
				}
			}
			else if ( args[0].equalsIgnoreCase("-reportstale")) {
				if (args.length == 3) {
					reportStale = true;
					return true;
				}
			}
			else if ( !args[0].equalsIgnoreCase("-h") && !args[0].equalsIgnoreCase("-help")) {
				System.out.println("unknown option: " + args[0]);
			}
		}
		usage();
		return false;
	}	

}
