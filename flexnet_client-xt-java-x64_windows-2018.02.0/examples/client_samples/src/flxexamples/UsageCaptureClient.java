/**
 * Copyright (c) 2013-2018 Flexera Software LLC.
 * All Rights Reserved.
 * This software has been provided pursuant to a License Agreement
 * containing restrictions on its use.  This software contains
 * valuable trade secrets and proprietary information of
 * Flexera Software LLC and is protected by law.
 * It may not be copied or distributed in any form or medium, disclosed
 * to third parties, reverse engineered or used in any manner not
 * provided for in said License Agreement except with the prior
 * written authorization from Flexera Software LLC.
 */

package flxexamples;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import com.flexnet.licensing.client.ICapabilityRequestOptions;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.ICapabilityResponseData;
import com.flexnet.licensing.client.IResponseStatus;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.SharedConstants.MachineType;
import com.flexnet.lm.SharedConstants.PropStatusCode;
import com.flexnet.lm.SharedConstants.RequestOperation;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.net.Comm;
import com.flexnet.lm.resources.Resources;


public class UsageCaptureClient {
	
	
//	This example program illustrates how to:
//	
//	1. Send a capability request describing a usage-capture report, 
//	   request, or undo operation to a server.
//	2. Depending on the operation type, process the response.
//	
//	These communications can be performed directly over HTTP or using
//	intermediate files for offline operations.

	public static void main(String[] args) {

		// Validate command-line arguments
		if(!validateArgs(args)) {
			return;
		}
		
		if(IdentityClient.IDENTITY_DATA == null) {
			System.out.println("License-enabled code requires client identity data, " +
								"which you create with pubidutil and printbin -java. " +
								"See the User Guide for more information.");
			return;
		}

		// Initialize ILicensing interface with identity data using file-based trusted storage in
		// user's home directory and hard-coded string hostid "1234567890" inside
		// a try-with-resources block
		try (ILicensing licensing = LicensingFactory.getLicensing(IdentityClient.IDENTITY_DATA, System.getProperty("user.home"), "1234567890", "usagecaptureclient")) {
			// Get ILicenseManager interface, the primary object for licensing-related functionality
			ILicenseManager licenseManager = licensing.getLicenseManager();
			
			// Existing features from trusted storage
			List<IFeature> existingFeatures = licenseManager.getFeaturesFromTrustedStorage(false);
			System.out.println("Number of features loaded from trusted storage: " + existingFeatures.size());
			
			byte[]			requestData = null;
			byte[]			responseData = null;

			// Generate a capability request
			if(generateRequest) {
				// Get request options
				ICapabilityRequestOptions	options = licenseManager.createCapabilityRequestOptions();
				// Desired feature
				if (operation != RequestOperation.UNDO) { 
					options.addDesiredFeature("survey", "1.0", 1);
				}
				// Correlation ID
				if (correlationId != null) {
					options.setCorrelationId(correlationId);
					System.out.println("Request correlation ID: " + correlationId);
				}
				// Rights ID
				if (rightsId != null) {
					options.addRightsId(rightsId,  1);
				}
				// Requestor ID
				options.setRequestorId("Example Application");
				// Acquisition ID
				options.setAcquisitionId("High resolution surveying");
				// Enterprise ID
				options.setEnterpriseId("5551212");
				// Request operation
				options.setRequestOperation(operation);
				// Generate request
				requestData = licenseManager.generateCapabilityRequest(options);
				if(storeRequest) {
					Files.write(Paths.get(outputFile), requestData, StandardOpenOption.CREATE_NEW);
					System.out.println("Successfully generated request to " + outputFile + ".");
				}
			}
			// Communicate with server
			if(server) {
				responseData = talkToServer(requestData, serverUrl);
			}

			// Read response from file
			if(readResponse) {
				System.out.println("Reading response from " + inputFile + ".");
				responseData = Files.readAllBytes(Paths.get(inputFile));
			}

			if(processResponse && (responseData != null) && (responseData.length > 0)) {
				// Process response
				ICapabilityResponseData responseDetails = licenseManager.processCapabilityResponse(responseData);
				System.out.println("Successfully processed response.");
				if(responseDetails != null) {
					// Get response details
					getResponseDetails(responseDetails);
				}
	            // Features in processed response
	            List<IFeature> features = licenseManager.getFeaturesFromTrustedStorage(false);
	            System.out.println("Number of features loaded from capability response: " + features.size());

				// Acquire 1 "survey" license
				acquireLicense(licenseManager, "survey", "1.0", 1);
			}
		}
		catch (FlxException e) {
			System.out.println("Licensing exception: " + e.getDiagnosticMessage());
		}
		catch (UnsatisfiedLinkError e) {
			// Make sure the native FlxCore library is available
			System.out.println("Failed to load FlxCore library. Ensure it's in PATH (Windows) or LD_LIBRARY_PATH (other platforms).");
		}
		catch (NoSuchFileException e) {
			System.out.println("No such file: " + e.getMessage());
		}
		catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
	}
	
	private static void getResponseDetails(ICapabilityResponseData responseDetails) {
		try {
			System.out.println("Response data");
			
			// Correlation ID
			System.out.println("Correlation ID:");
			if (responseDetails.getCorrelationId() != null) {
				System.out.println("\t" + responseDetails.getCorrelationId());
			}
			else {
				System.out.println("\tnone");
			}

			// Vendor dictionary
			System.out.println("Vendor dictionary:");
			Map<String, Object>	vendorDictionary = responseDetails.getVendorDictionary();
			if(vendorDictionary.size() > 0) {
				Set<String> keys = vendorDictionary.keySet();
				for(String key : keys) {
					Object	value = vendorDictionary.get(key);
					System.out.println("\t" + key + "=" + value.toString());
				}
			}
			else {
				System.out.println("\tnone");
			}

			// Status code
			System.out.println("Response status:");
			List<IResponseStatus> statusCodes = responseDetails.getResponseStatus();
			if(statusCodes.size() > 0) {
				for(IResponseStatus status : statusCodes) {
					PropStatusCode code = PropStatusCode.findId(status.getCode());
					String description = Resources.getString(code.getKey());
					System.out.println("\t" + "category: " + status.getCategory().toString().toLowerCase() + 
							", code: " + status.getCode() + (description == null ? "" : " (" + description + ")") +
							", details: " + status.getDetails());
				}
			}
			else {
				System.out.println("\tnone");
			}

			// Virtual machine info
			System.out.println("Machine Type:");
			MachineType	machineType = responseDetails.getVirtualMachineType();
			if(machineType == MachineType.VIRTUAL) {
				System.out.println("\tVIRTUAL");
				Map<String, Object> vmInfo = responseDetails.getVirtualMachineInfo();
				if(vmInfo.size() > 0) {
					Set<String> keys = vmInfo.keySet();
					for(String key : keys) {
						Object value = vmInfo.get(key);
						System.out.println("\t\t" + key + "=" + value.toString());
					}
				}
			}
			else if(machineType == MachineType.PHYSICAL) {
				System.out.println("\tPHYSICAL");
			}
			else {
				System.out.println("\tUNKNOWN");
			}

		}
		catch (FlxException e) {
			System.out.println("Error getting response details.");
		}
	}


	private static byte[] talkToServer(byte[] request, String uri) {
	    byte[] response = null;
	    try {
	        Comm conn = Comm.getHttpInstance(uri);
	        response = conn.sendBinaryMessage(request);
	    }
	    catch (FlxException e) {
	        e.printStackTrace();
	    }
	    return response;
	}

	private static void acquireLicense(ILicenseManager licenseManager, String name, String version, int count) {
		ILicense	license = null;
		try {
			license = licenseManager.acquire(name,  version, count);
			System.out.println("Successfully acquired \"" + license.getName() + "\", version " +
					license.getVersion() +  ", " + license.getCount() + " count.");
			try {
				// return non-metered and release all metered license(s)
				licenseManager.returnAllLicenses();
			}
			catch (FlxException e) {
				System.out.println("Unable to return " + name + ": " + e.getMessage());
			}
		}
		catch (FlxException e) {
			System.out.println("Unable to acquire " + name + ": " + e.getMessage());
		}
	}
	
	private static boolean validateArgs(String[] args) {
		if (args.length == 0) {
			usage();
			return false;
		}
		for (int ii = 0; ii < args.length; ii++) {
			String option = args[ii];
			
			if (option.equalsIgnoreCase("-request")) {
				operation = RequestOperation.REQUEST;
			}
			else if (option.equalsIgnoreCase("-report")) {
				operation = RequestOperation.REPORT;
			}
			else if (option.equalsIgnoreCase("-undo")) {
				operation = RequestOperation.UNDO;
			}
			else if (option.equalsIgnoreCase("-correlation") && (ii + 1) < args.length) {
				correlationId = args[++ii];
			}
			else if (option.equalsIgnoreCase("-rightsid")) {
				rightsId = args[++ii];
			}
			else if (option.equalsIgnoreCase("-server") && (ii + 1) < args.length) {
				serverUrl = args[++ii];
				generateRequest = true;
				processResponse = true;
				server = true;
			}
			else if (option.equalsIgnoreCase("-generate") && (ii + 1) < args.length) {
				outputFile = args[++ii];
				generateRequest = true;
				storeRequest = true;
			}
			else if (option.equalsIgnoreCase("-process") && (ii + 1) < args.length) {
				inputFile = args[++ii];
				readResponse = true;
				processResponse = true;
			}
			else {
				if (!option.equalsIgnoreCase("-h") && !option.equalsIgnoreCase("-help")) {
					System.out.println("unknown option: " + option);	
				}
				usage();
				return false;
			}
		}
		// validate arguments
		if (!generateRequest && !storeRequest && !readResponse && !server && !processResponse) {
			// nothing to do
			usage();
			return false;
		}
		else if (operation == RequestOperation.UNDO && correlationId == null) {
			System.out.println("Undo operation requires a correlation ID.");
			return false;
		}
		else if (operation == RequestOperation.REPORT && rightsId != null) {
			System.out.println("Report operation is not compatible with rightsid option.");
			return false;
		}
		return true;  
	}
	
	private static void usage() {
		System.out.println(
				"\nUsageCaptureClient [-request|-report|-undo] [-correlation id] [-rightsid rights-id]" +
				"\n                   [-server url|-generate outputfile|-process inputfile]" +
				"\n\nwhere:" +
				"\n-request  Request operation.  This is the default operation type if unspecified." +
				"\n-report   Report operation." +
				"\n-undo     Undo operation.  Correlation ID must be set with -correlation." +
				"\n" +
				"\n-correlation    Sets the correlation ID." +
				"\n" +
				"\n-rightsid    Sets the rights ID." +
				"\n" +
				"\n-server   Sends usage-capture message to a server and processes the response." +
				"\n-generate Generates usage-capture message into a file." +
				"\n-process  Processes server's response from a file.");
	} 
	
	private static String	correlationId;
	private static String	rightsId;
	private static RequestOperation	operation = RequestOperation.REQUEST;
	private static String	serverUrl;
	private static String	inputFile;
	private static String	outputFile;
	private static boolean	generateRequest;
	private static boolean	storeRequest;
	private static boolean	readResponse;
	private static boolean	server;
	private static boolean	processResponse;
}

