package calc_tra;
/**
 * Copyright (c) 2011-2018 Flexera Software LLC. All Rights Reserved. This software has been
 * provided pursuant to a License Agreement containing restrictions on its use. This software
 * contains valuable trade secrets and proprietary information of Flexera Software LLC and is
 * protected by law. It may not be copied or distributed in any form or medium, disclosed to third
 * parties, reverse engineered or used in any manner not provided for in said License Agreement
 * except with the prior written authorization from Flexera Software LLC.
 */

import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.lm.FlxException;
import org.luaj.vm2.Varargs;
import com.flexnet.tra.VarArgFunction;
import org.luaj.vm2.LuaValue;
import com.flexnet.tra.TRA_callable;
public class Acquire {
	public static class acquireLicense extends VarArgFunction {
		public Varargs invoke(Varargs v){
			CalculatorUserData	user_data = (CalculatorUserData)v.arg(1).checkuserdata();
			ILicensing			licensing = user_data.get_licensing();
			ILicenseManager		licenseManager = user_data.get_licenseManager();
			TRA_callable 		tra = user_data.get_tra();
			ILicense			license = null;
			try {
				System.out.println("Try acquire \"" + tra.get_string(user_data.get_name_alias() ) + "\", version " + tra.get_string(user_data.get_version_alias() ) +  ", 1  count.");
				license = licenseManager.acquire(
					tra.get_string(user_data.get_name_alias() ),
					tra.get_string(user_data.get_version_alias() ),
					1);
				System.out.println("Successfully acquired \"" + license.getName() + "\", version " +
						license.getVersion() +  ", " + license.getCount() + " count.");
				try {
					// return license
					licenseManager.returnLicense(license);
					return LuaValue.valueOf( true );
				}
				catch (FlxException e) {
					System.out.println("Unable to return " + name + ": " + e.getMessage());
				}
			}
			catch (FlxException e) {
				System.out.println("Unable to acquire " + name + ": " + e.getMessage());
			}
			return LuaValue.valueOf( false );
		}
	}

	public static class on_acquire extends VarArgFunction {
		public Varargs invoke(Varargs v){
			CalculatorUserData	user_data = (CalculatorUserData)v.arg(1).checkuserdata();
			TRA_callable 		tra = user_data.get_tra();
			System.out.println("Flag that we are licensed");
			tra.set_value(
				user_data.get_licensed_alias_1(),
				1 + tra.get_value( user_data.get_licensed_alias_2())
			);
			return LuaValue.valueOf( true );
		}
	}

	public static class unexpected extends VarArgFunction {
		public Varargs invoke(Varargs v){
			System.out.println("Ignore that we are not licensed");
			return LuaValue.valueOf( true );
		}
	}

}
