package calc_tra;
/**
 * Copyright (c) 2011-2018 Flexera Software LLC. All Rights Reserved. This software has been
 * provided pursuant to a License Agreement containing restrictions on its use. This software
 * contains valuable trade secrets and proprietary information of Flexera Software LLC and is
 * protected by law. It may not be copied or distributed in any form or medium, disclosed to third
 * parties, reverse engineered or used in any manner not provided for in said License Agreement
 * except with the prior written authorization from Flexera Software LLC.
 */
 
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.tra.TRA_callable;
// 				tra.get_string(Calculator_tra.TRA_STRING_HIGHRES_ALIAS_1),
// 				tra.get_string(Calculator_tra.TRA_STRING_VERSION_ALIAS_1),

public class CalculatorUserData {
	public CalculatorUserData(
		ILicensing		l,
		ILicenseManager	m,
		TRA_callable	t,
		int				n,
		int				v,
		int				a1,
		int				a2
	) {
		licensing = l;
		licenseManager = m;
		tra = t;
		name_alias = n;
		version_alias = v;
		licensed_alias_1 = a1;
		licensed_alias_2 = a2;
	}
	public ILicensing get_licensing() { return licensing; }
	public ILicenseManager get_licenseManager() { return licenseManager; }
	public TRA_callable get_tra() { return tra; }
	public int get_name_alias() { return name_alias; }
	public int get_version_alias() { return version_alias; }
	public int get_licensed_alias_1() { return licensed_alias_1; }
	public int get_licensed_alias_2() { return licensed_alias_2; }

	private ILicensing		licensing = null;
	private ILicenseManager	licenseManager = null;
	private TRA_callable	tra = null;
	private final int name_alias;
	private final int version_alias;
	private final int licensed_alias_1;
	private final int licensed_alias_2;
}
