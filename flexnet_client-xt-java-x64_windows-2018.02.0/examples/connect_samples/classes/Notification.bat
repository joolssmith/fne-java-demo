@echo off
set "CMD_LINE_ARGS= "
:setupArgs
if "%~1" == "" goto :doneArgs
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift /1
goto :setupArgs
:doneArgs

:: figure out where things are

set SCRIPT_DIR=%~dp0
set LIB=%SCRIPT_DIR%\..\..\..\lib
set CLASSES=%SCRIPT_DIR%

:: find Java

set JAVA=%JAVA_HOME%\bin\java.exe
:: Use JAVA_HOME?
if exist "%JAVA%" goto :javaOK
:: Just find java in the path
set JAVA=java
:javaOK

"%JAVA%" -cp "%CLASSES%;%LIB%\flxConnect.jar;%LIB%\flxConnectClient.jar;%LIB%\flxBinary.jar;%LIB%\flxClient.jar;%LIB%\flxClientNative.jar;%LIB%\commons-codec-1.9.jar;%LIB%\jna-4.1.0.jar;%LIB%\jna-platform-4.1.0.jar;%LIB%\commons-net-3.3.jar;%LIB%\httpclient-4.5.jar;%LIB%\httpclient-cache-4.5.jar;%LIB%\httpclient-win-4.5.jar;%LIB%\httpcore-4.4.1.jar;%LIB%\httpmime-4.5.jar;%LIB%\cfluent-hc-4.5.jar;%LIB%\commons-logging-1.2.jar;" -Djava.library.path="%LIB%" connectexamples.Notification %CMD_LINE_ARGS%
