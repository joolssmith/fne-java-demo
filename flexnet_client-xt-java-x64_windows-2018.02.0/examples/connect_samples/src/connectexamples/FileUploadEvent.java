package connectexamples;

/****************************************************************************
Copyright (c) 2015-2018 Flexera Software LLC.
All Rights Reserved.
This software has been provided pursuant to a License Agreement
containing restrictions on its use.  This software contains
valuable trade secrets and proprietary information of
Flexera Software LLC and is protected by law.
It may not be copied or distributed in any form or medium, disclosed
to third parties, reverse engineered or used in any manner not
provided for in said License Agreement except with the prior
written authorization from Flexera Software LLC.
*****************************************************************************/

import java.util.ArrayList;

import com.flexnet.connect.client.CommSettings;
import com.flexnet.connect.client.interfaces.ConnectFactory;
import com.flexnet.connect.client.interfaces.FileUploadEventFactory;
import com.flexnet.connect.client.interfaces.ICommSettings;
import com.flexnet.connect.client.interfaces.IConnect;
import com.flexnet.connect.client.interfaces.IFileUploadEventRequest;
import com.flexnet.connect.client.interfaces.IFileUploadInfo;
import com.flexnet.connect.client.interfaces.IProduct;
import com.flexnet.connect.client.interfaces.IUploadUpdateStatusCallback;
import com.flexnet.connect.client.interfaces.IUploadUpdateStatusUserdata;
import com.flexnet.connect.client.interfaces.IUploadUpdateStatusUserdata.UploadUpdateStage;
import com.flexnet.connect.client.interfaces.ProductFactory;
import com.flexnet.lm.FlxException;

public class FileUploadEvent {

	/* FlexNet Connect Notification Server */
	private static String	serverName = "http://updatesuat.flexnetoperations.com";
	
	/* Product-related info */
	private static String	code = "{77777777-7777-7777-7777-777777777777}";
	private static String	version="1.0", language="1033", platform="WIN64";
	
	/* Test environment */
	private static String	storageLocation = System.getProperty("user.home");
	
	/* Update Status callback stuff */
	private static FileUploadEvent testFileUploadEventObject = new FileUploadEvent();
	private static IUploadUpdateStatusUserdata 		uploadStatusUserdata = testFileUploadEventObject.new UploadUpdateStatusUserdata();
	private static IUploadUpdateStatusCallback 		uploadStatusCallback = testFileUploadEventObject.new UploadUpdateStatusCallback();
	
	/* Pause percent */
	private static int userDefinedPauseAtPercentage = 0;
	
	/* Flags */
	private static boolean unregister = false;
	
	/* File Upload */
	private static String	uploadServer = null;
	private static String	ftpUsername = null, ftpPassword = null;
	private static boolean	customCommUpload = false, wasPaused = false;
	private static String	localFile = null, destPath = null, destFilename = null, fileType = null;
	private static String	customAttrNameStr = null, customAttrNameInt = null, customAttrValStr = null;
	private static int		customAttrValInt = 0;
	
	/* The Connect object and Product object(s) */
	private static IConnect	connect = null;
	private static IProduct	product1 = null;
	private static IFileUploadEventRequest fileUploadEvent1 = null;
	
	public static void main(String[] args) 
	{
		/* Data needed for creating a Connect object */
		String 		customHostId = null;
		
		boolean		success = true;
		
		/* Validate command-line arguments */
		if(!validateArgs(args)) return;
		
		try
		{
			/* Use the ConnectFactory class to create a Connect object. */
			connect = ConnectFactory.connectCreate(IdentityClient.IDENTITY_DATA, storageLocation, customHostId);
			
			/* Before creating and registering a product, check if it has already been registered
			 * (using getProductReference) and if so, return the registered product.  Else, register the product. */
			
			if (connect.isProductRegistered(code)) {
				product1 = connect.getRegisteredProduct(code);
			}
			else {
				/* Use the ProductFactory class to create a Product object and register it with the Connect object */
				product1 = ProductFactory.productCreate(connect, code, version, language, platform, serverName);
				
				/* Register the product */
				connect.registerProduct(product1);
			}
			
			/* Print status */
			printProductInfo();
			
			/* Set the product's notification server */
			product1.setNotificationServer(serverName);
			
			testFileUploadEvent ();
			
			/* Resume the upload if it was paused. */
			if (wasPaused) 
			{
				System.out.println ("\nResuming a paused upload from " + userDefinedPauseAtPercentage + " percent...");
				userDefinedPauseAtPercentage = 0;
				product1.handleEvent(fileUploadEvent1, uploadStatusUserdata, uploadStatusCallback);
			}
			
		}
		catch (FlxException e)
		{
			printException(e);
			success = false;
		}
		
		if (unregister)
		{
			/* Unregister the product */
			System.out.println ("Unregistering the product... ");
			try 
			{
				connect.unregisterProduct(product1);
				System.out.print("Done.");
				System.out.println();
			} 
			catch (FlxException e) 
			{
				printException(e);
				success = false;
			}
		}
		
		if (success)
		{
			System.out.println();
			System.out.println ("Completed all FlexNet Connect 'File Upload Event' demonstrations.");
			System.out.println ("Please see client logs and/or server for details.");
		}
		
		return;
	}

	
	
	private static boolean validateArgs(String[] args)
	{
	    boolean     invalidSpec = false;
	    boolean     unknownArg  = false;
	    int    		ii = 0, numArgs = 0;;
	    String	    arg = null;
	    
	    numArgs = args.length;

	    for (ii = 0; !invalidSpec && ii < numArgs; ii++)
	    {
	    	arg = args[ii];
	        if (arg.equalsIgnoreCase("-help") || arg.equalsIgnoreCase("-h"))
	        {
	            usage();
	            return false;
	        }
	        else if (arg.equalsIgnoreCase("-server"))
	        { /* should be followed by 1 parameter for notification server url */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	serverName = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productid"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	                code = args[++ii];
	    			if(code.length() != 38)
	    			{
	    	            System.out.println("Invalid product code specified: " + code);
	    	            return false;
	    			}
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productver"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	version = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productlang"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	language = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productplat"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	platform = args[++ii];
	            }
	        }	        
	        else if (arg.equalsIgnoreCase("-unregister"))
	        { 
	        	unregister = true;
	        }
	        else if (arg.equalsIgnoreCase("-commupload"))
	        { 
	        	customCommUpload = true;
	        }
	        else if (arg.equalsIgnoreCase("-uploadserver"))
	        { /* should be followed by 1 parameter for the file upload server */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	uploadServer = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-ftpusername"))
	        { /* should be followed by 1 parameter for the FTP file upload server username */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	ftpUsername = args[++ii];
	            	customCommUpload = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-ftppassword"))
	        { /* should be followed by 1 parameter for the FTP file upload server password */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	ftpPassword = args[++ii];
	            	customCommUpload = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-localfile"))
	        { /* should be followed by 1 parameter for the path to the local file */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	localFile = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-destpath"))
	        { /* should be followed by 1 parameter for path at the destination file upload server*/
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	destPath = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-destfilename"))
	        { /* should be followed by 1 parameter for filename on the destination file upload server */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	destFilename = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-filetype"))
	        { /* should be followed by 1 parameter for filetype */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	fileType = args[++ii];
	            }
	        }
	        else
	        {
	            invalidSpec = true;
	            unknownArg = true;
	        }
	    }

	    if (invalidSpec)
	    {
	        if (unknownArg)
	        {
	            System.out.println("unknown option: " + arg);
	        }
	        else
	        {
	            System.out.println("invalid specification for option: " + arg);
	        }
	        
	        usage();
	        return false;
	    }

	    return true;
	}
	
	
	private static void usage()
	{
		System.out.println("This example will download any available Notifications for the product.  " +
						 "\nIt can then download the payload indicatated by the Notification " +
						 "\nand also proceed to execute the downloaded file.");
		System.out.println();
		System.out.println("Notification" + " [-server url] [-productid product_id] [-productver product_version] [-productlang product_language] [-productplat product_platform]");
		System.out.println();
		System.out.println("-server         Url of the FlexNet Connect notification server.");
		System.out.println("                Default is " + serverName);
		System.out.println("-productid      FlexNet Connect id of product to upgrade.");
		System.out.println("                Default is " + code);
		System.out.println("-productver     FlexNet Connect version of product to upgrade.");
		System.out.println("                Default is " + version);
		System.out.println("-productlang    FlexNet Connect language of product to upgrade.");
		System.out.println("                Default is " + language);
		System.out.println("-productplat    FlexNet Connect product's platform.");
		System.out.println("                Default is " + platform);
		System.out.println("-uploadserver   Client-specified file upload destination.");
		System.out.println("                Default is " + uploadServer);
		System.out.println("-ftpusername    FTP username for the file upload server.");
		System.out.println("                Default is " + ftpUsername);
		System.out.println("-ftppassword    FTP password for the file upload server.");
		System.out.println("                Default is " + ftpPassword);
		System.out.println("-localfile      Full local path and filename of the file to be uploaded.");
		System.out.println("                Default is " + localFile);
		System.out.println("-destpath       User-specified file storage path at destination.");
		System.out.println("                Default is " + destPath);
		System.out.println("-destfilename   Client-specified filename at destination.");
		System.out.println("                Default is name of local file" );
		System.out.println("-filetype       Type of file.");
		System.out.println("                Default is " + fileType);
		
		System.out.println("-unregister  Unregisters the product after all tests are completed.");
		System.out.println();
	}

	private static void printProductInfo ()
	{
		System.out.println();
		System.out.println ("FlexNet Connect has successfully registered the following product - ");
		System.out.println ("Product Code  : " + code);
		System.out.println ("Version       : " + version);
		System.out.println ("Language      : " + language);
		System.out.println ("Platform      : " + platform);
		System.out.println ("Server        : " + serverName);
		System.out.println ("Data location : " + storageLocation);
		System.out.println();
	}
	
	
	private static void testFileUploadEvent() throws FlxException
	{
		ICommSettings commSettings = null;
		
		/* Create a custom ICommSettings object if required */
		if (customCommUpload) commSettings = new CommSettings();
		if ((ftpUsername != null) && (ftpPassword != null)) commSettings.setFtpCredentials(ftpUsername, ftpPassword);
		
		/* Create the event */
		fileUploadEvent1 = FileUploadEventFactory.fileUploadEventCreate(product1, localFile, fileType, 
				uploadServer, commSettings);
		
		/* Set the custom destination path / filename, if provided */
		if (destPath != null) fileUploadEvent1.getFile(0).setCustomPath(destPath);
		if (destFilename != null) fileUploadEvent1.getFile(0).setCustomFilename(destFilename);
		
		/* Add custom attributes, if provided */
		if (customAttrNameStr != null) fileUploadEvent1.getFile(0).addCustomAttribute(customAttrNameStr, customAttrValStr);
		if (customAttrNameInt != null) fileUploadEvent1.getFile(0).addCustomAttribute(customAttrNameInt, customAttrValInt);
		
		/* Perform the upload */
		product1.handleEvent(fileUploadEvent1, uploadStatusUserdata, uploadStatusCallback);
		
		/* Print the results */
		printFileUploadResults (fileUploadEvent1);
	}
		

	private class UploadUpdateStatusCallback implements IUploadUpdateStatusCallback{

		public UploadUpdateStatusCallbackReturnType uploadUpdateStatusCallbackMethod(UploadUpdateStage stage, 
				IFileUploadInfo fileUploadInfo, long currentSize, long totalSize, IUploadUpdateStatusUserdata userData)
		{
			/* Do whatever you want here to display progress */
			
			long percentComplete = 0;
			
			/* Display change in state */
			if (stage != ((UploadUpdateStatusUserdata)userData).lastUpdateStage)
			{
				((UploadUpdateStatusUserdata)userData).lastUpdateStage = stage;
				if (stage == UploadUpdateStage.UPLOAD_STAGE_START) System.out.println("Upload started: " + fileUploadInfo.getFileName());
				else if (stage == UploadUpdateStage.UPLOAD_STAGE_END)
				{
					System.out.println();
					System.out.println("Upload ended: " + fileUploadInfo.getFileName());
				}
			}
			
			if (stage == UploadUpdateStage.UPLOAD_STAGE_UPLOADING)
			{
				if (totalSize > 0)
				{
					percentComplete = (long)(((double)((double)currentSize / (double)totalSize)) * 100);
					if (percentComplete > ((UploadUpdateStatusUserdata)userData).lastPercentComplete)
					{
						int 					progressBarWidth = 50;
						int 					progressBarPos = 0;	
						
						progressBarPos = (int)(progressBarWidth * ((double)currentSize / (double)totalSize));
						System.out.print("\r");
						System.out.print("[");
						for (int i = 0; i < progressBarWidth; ++i) 
						{
							if (i < progressBarPos) System.out.print("=");
							else if (i == progressBarPos) System.out.print(">");
							else System.out.print(" ");
						}
						if (percentComplete >= 100) System.out.print("]");
						System.out.print(" " + percentComplete + " %");
						
						((UploadUpdateStatusUserdata)userData).lastPercentComplete = percentComplete;
					}
				}
				else
				{
					System.out.print("\r");
					System.out.print("Uploading.  Please wait...");
				}
			}
			
			UploadUpdateStatusCallbackReturnType	callbackReturnValue = UploadUpdateStatusCallbackReturnType.FLC_UPLOAD_STATUS_RETURN_CONTINUE;
			
			if ((userDefinedPauseAtPercentage > 0) && (percentComplete >= userDefinedPauseAtPercentage))
			{
				/* Simulating a simple scenario to test 'Pause' */
				System.out.println ("\nUpload paused at " + userDefinedPauseAtPercentage + " percent.");
				callbackReturnValue = UploadUpdateStatusCallbackReturnType.FLC_UPLOAD_STATUS_RETURN_PAUSE;
				wasPaused = true;
			}
			else if ((totalSize > 0) && (currentSize > totalSize))
			{
				/* Something's odd.  If you wish to cancel the download, uncomment the line below.*/
				// callbackReturnValue = DownloadUpdateStatusCallbackReturnType.FLC_UPDATE_STATUS_RETURN_CANCEL;
			}
			
			return callbackReturnValue;
		}
	}
	
	
	private class UploadUpdateStatusUserdata implements IUploadUpdateStatusUserdata{

		/* Customize the user data object you'd like passed through the Update Status callback
		 * mechanism to suit your needs. */
		
		public long lastPercentComplete;
		public UploadUpdateStage lastUpdateStage;

		public UploadUpdateStatusUserdata()
		{
			lastPercentComplete = 0;
			lastUpdateStage = UploadUpdateStage.UPLOAD_STAGE_IDLE;
		}
	}
	
	private static void printFileUploadResults (IFileUploadEventRequest fileUploadEvent)
	{
		ArrayList<IFileUploadInfo> fileList = fileUploadEvent.getFiles();
		int count = 1;
		
		for (IFileUploadInfo file : fileList)
		{
			System.out.println();
			System.out.println("File Upload Information:   Upload #" + count);
			System.out.println("       Client Location:           " + file.getFileName());
			System.out.println("       File Type:                 " + file.getFileType());
			System.out.println("       Client Upload Server:      " + file.getDestClient());
			System.out.println("       Server Upload Server:      " + file.getDestServer());
			System.out.println("       Upload Location:           " + file.getDestUploaded());
		}
	}
	
	private static void printException (FlxException e)
	{
		System.out.println("ERROR!");
		System.out.println(e.getMessage());
		
		if (e.getArguments() != null)
		{
			Object exceptionArgs[] = new Object[e.getArguments().length];
	        exceptionArgs = e.getArguments();
	        for (Object arg: exceptionArgs)
	        {
	            System.out.println(arg.toString());
	        }
		}
      
		System.out.println("Stacktrace - ");
		e.printStackTrace();
	}
}
