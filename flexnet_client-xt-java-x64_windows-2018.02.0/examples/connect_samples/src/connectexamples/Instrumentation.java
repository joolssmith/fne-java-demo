/****************************************************************************
  Copyright (c) 2014-2018 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/

package connectexamples;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.flexnet.connect.client.interfaces.ConnectFactory;
import com.flexnet.connect.client.interfaces.IConnect;
import com.flexnet.connect.client.interfaces.IInstrumentationEvent;
import com.flexnet.connect.client.interfaces.IInstrumentationLog;
import com.flexnet.connect.client.interfaces.IProduct;
import com.flexnet.connect.client.interfaces.InstrumentationEventFactory;
import com.flexnet.connect.client.interfaces.ProductFactory;
import com.flexnet.lm.FlxException;

public class Instrumentation {

	/* FlexNet Connect Notification Server */
	private static String	serverName = "http://updatesuat.flexnetoperations.com";
	
	/* Product-related info */
	private static String	code = "{77777777-7777-7777-7777-777777777777}";
	private static String	version="1.0", language="1033", platform="WIN64";
	private static String 	storageLocation = System.getProperty("user.home");
	
	private static IProduct	product1 = null;
	
	private static String	group1 = "Test Inst Group 1", action1 = "Test Event Action 1", value1 = "Test Inst Value 999";
	private static String	dictName1 = "Dictionary Name 1", dictName2 = "Dictionary Name 2", dictValueText = "Dictionary Value Text";
	private static String	instanceIdentifier = null;
	private static int		maxUploadPacketSize = 800;
	private static long		maxLogSize = 0x10000;
	
	private static boolean	test = false, upload = false, purge = false, unregister = false;
	private static boolean	eventUpload = false;
	private static boolean	customInstEvent = false;
	private static boolean	customDictData = false;
	
	public static void main(String[] args) 
	{
		/* The Connect object and Product object(s) */
		IConnect	connect = null;
		
		/* Data needed for creating a Connect object */
		String customHostId = "1234567890";
		
		boolean		success = true;
		
		/* Validate command-line arguments */
		if(!validateArgs(args)) return;
		
		try
		{
			/* Use the ConnectFactory class to create a Connect object. */
			connect = ConnectFactory.connectCreate(IdentityClient.IDENTITY_DATA, storageLocation, customHostId);
			
			/* Before creating and registering a product, check if it has already been registered
			 * (using getProductReference) and if so, return the registered product.  Else, register the product. */
			
			if (connect.isProductRegistered(code)) {
				product1 = connect.getRegisteredProduct(code);
			}
			else {
				/* Use the ProductFactory class to create a Product object and register it with the Connect object */
				product1 = ProductFactory.productCreate(connect, code, version, language, platform, serverName);
				
				/* Register the product */
				connect.registerProduct(product1);
			}
			
			/* Print status */
			printProductInfo();
			
			/* Set the product's notification server */
			product1.setNotificationServer(serverName);
			
			/* Set the max Instrumentation Log size */
			product1.setMaxInstrumentationLogSize(maxLogSize);
			
			/* Run Instrumentation Examples */
			if (test)
			{
				runInstrumentationExamples(connect, product1);
				uploadLog();
			}
			
			/* Store the custom Instrumentation Event */
			if (customInstEvent) storeCustomEvent (product1);
			
			/* Upload the log */
			if (upload) uploadLog();
			
			/* Purge logs */
			if (purge) purgeLogs();
			
		}
		catch (FlxException e)
		{
			printException(e);
			success = false;
		}

		if (unregister)
		{
			/* Unregister the product */
			System.out.println ("Unregistering the product... ");
			try 
			{
				connect.unregisterProduct(product1);
				System.out.print("Done.");
				System.out.println();
			} 
			catch (FlxException e) 
			{
				printException(e);
				success = false;
			}
		}
		
		if (success)
		{
			System.out.println();
			System.out.println ("Successfully completed all FlexNet Connect 'Instrumentation' demonstrations.");
		}
		
		return;
	}

	private static void runInstrumentationExamples (IConnect connect, IProduct product)
	{
		String					group2 = "Test Inst Group 2", action2 = "Test Event Action 2", value2 = "Test Inst Value 2";
		byte					dictValueBinary[] = {0x00, 0x01, 0x02, 0x03, 0x04};
		int						numLogEntries = 0;
		long					logSize = 0;
		IInstrumentationEvent	testEvent1 = null, testEvent2 = null;
		IInstrumentationLog		testLog = null;
		Map<String, IInstrumentationLog> logInstances;
		
		try
		{
			/* Create an Instrumentation Event.  This event will be uploaded to the server. */
			System.out.println("Creating event 1 - Group: " + group1 + " Action: " + action1 + " Value: " + value1);
			testEvent1 = InstrumentationEventFactory.instrumentationEventCreate(group1, action1, value1, product);
		
			/* Add a data dictionary item (text) to the event */
			System.out.println("Adding dictionary data to event 1 (text) - Name: " + dictName1 + " Value: " + dictValueText);
			testEvent1.addDataDictionaryItem(dictName1, dictValueText);
			
			/* Add a data dictionary item (binary) to the event */
			System.out.println("Adding dictionary data to event 1 (binary) - Name: " + dictName2 + " Value: " + "<Binary>");
			testEvent1.addDataDictionaryItem(dictName2, dictValueBinary);
			
			/* Upload the event */
			System.out.println("Uploading the event...");
			if (eventUpload) testEvent1.upload(connect, product);
			
			/* Create a second event that will be logged  */
			System.out.println("Creating event 2 - Group: " + group2 + " Action: " + action2 + " Value: " + value2);
			testEvent2 = InstrumentationEventFactory.instrumentationEventCreate(group2, action2, value2, product);
			
			/* Give it a couple of dictionary items */
			System.out.println("Adding dictionary data to event 2 (text) - Name: " + dictName1 + " Value: " + dictValueText);
			testEvent2.addDataDictionaryItem(dictName1, dictValueText);
			
			/* Add a data dictionary item (binary) to the event */
			System.out.println("Adding dictionary data to event 2 (binary) - Name: " + dictName2 + " Value: " + "<Binary>");
			testEvent2.addDataDictionaryItem(dictName2, dictValueBinary);
			
			/* Retrieve the product's default (i.e. non-custom instance) Instrumentation Log */
			System.out.println("Retrieving log");
			testLog = product.getInstrumentationLog(instanceIdentifier);
			
			/* Log testEvent2 (log several events for testing) */
			for (int i=0; i < 1; i++)
			{
				System.out.println ("Logging event 1");
				testLog.write(testEvent1);
				System.out.println ("Logging event 2");
				testLog.write(testEvent2);
			}
			
			/* Get the size of the log */
			logSize = testLog.getSize();
			
			/* Get the number of entries in the log */
			numLogEntries = testLog.getNumEntries();
			
			/* Console output */
			System.out.println ("Log Size = " + logSize + " .  " + "# Entries = " + numLogEntries);
			
			/* Get the log instances */
			logInstances = product.getInstrumentationLogInstances();
			
			/* Console output */
			System.out.println(" Instrumentation Log Instances: ");
			Iterator<Entry<String, IInstrumentationLog>> it = logInstances.entrySet().iterator();
			while (it.hasNext())
			{
				Map.Entry<String, IInstrumentationLog> log = (Map.Entry<String, IInstrumentationLog>)it.next();
				System.out.println("\t" + log.getKey() + "\t : " + log.getValue().getFilepath());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
		return;
	}
	
	
	private static void storeCustomEvent (IProduct product)
	{
		IInstrumentationEvent	testEvent1 = null;
		IInstrumentationLog		testLog = null;
		
		try
		{
			/* Create an Instrumentation Event.  This event will be uploaded to the server. */
			testEvent1 = InstrumentationEventFactory.instrumentationEventCreate(group1, action1, value1, product);
		
			/* Add a data dictionary item (text) to the event */
			if (customDictData) testEvent1.addDataDictionaryItem(dictName1, dictValueText);

			/* Retrieve the product's default (i.e. non-custom instance) Instrumentation Log */
			testLog = product.getInstrumentationLog(instanceIdentifier);
			
			/* Write the event*/
			System.out.println("Storing custom Instrumentation Event - " + group1 + " " + action1 + " "+ value1
					+ " "+ dictName1 + " " + dictValueText);
			testLog.write(testEvent1);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
		return;
	}
	
	
	private static void uploadLog() throws FlxException
	{
		IInstrumentationLog		testLog = null;
		long 					uploadSize = 0;
		int						uploadNumEntries = 0, uploadEntriesRemaining = 0;
		String					instanceLabel = null;
		
		if (instanceIdentifier == null) instanceLabel = "Default";
		else instanceLabel = instanceIdentifier;
		
		testLog = product1.getInstrumentationLog(instanceIdentifier);
		
		if (testLog != null)
		{
			System.out.println("Uploading Instrumentation log " + instanceLabel);
			
			/* Upload the log */
			testLog.upload(testLog.getSize(), maxUploadPacketSize);
			
			/* Get info on the upload*/
			uploadSize 				= testLog.getUploadSize();
			uploadNumEntries		= testLog.getUploadNumEntries(); 
			uploadEntriesRemaining	= testLog.getNumEntries();
			
			System.out.println ("Upload completed.");
			System.out.println ("Last Upload: " + instanceLabel + "," + uploadSize + "," +
						uploadNumEntries + "," + uploadEntriesRemaining);
		}
		else
		{
			System.out.println("Instrumentation log " + instanceLabel + " for product " + code + " not found!");
		}
		return;
	}
	
	private static void purgeLogs() throws FlxException
	{
		Map<String, IInstrumentationLog> logInstances = null;
		
		/* Get the log instances */
		logInstances = product1.getInstrumentationLogInstances();
	
		if ((logInstances != null) && (logInstances.size() > 0))
		{
			/* Console output */
			System.out.println("Deleting Instrumentation logs... ");
			Iterator<Entry<String, IInstrumentationLog>> it = logInstances.entrySet().iterator();
			while (it.hasNext())
			{
				Map.Entry<String, IInstrumentationLog> log = (Map.Entry<String, IInstrumentationLog>)it.next();
				System.out.println("Deleting \t" + log.getKey() + "\t : " + log.getValue().getFilepath());
				product1.deleteInstrumentationLogInstance(log.getKey());
			}
			System.out.println("All Instrumentation logs for poduct " + code + "deleted.");
		}
		else
		{
			System.out.println("No Instrumentation logs found for poduct " + code + "!");
		}
		return;
	}
	
	
	private static boolean validateArgs(String[] args)
	{
	    boolean     invalidSpec = false;
	    boolean     unknownArg  = false;
	    int    		ii = 0, numArgs = 0;;
	    String	    arg = null;
	    
	    numArgs = args.length;

	    for (ii = 0; !invalidSpec && ii < numArgs; ii++)
	    {
	        arg = args[ii];
	        if (arg.equalsIgnoreCase("-help") || arg.equalsIgnoreCase("-h"))
	        {
	            usage();
	            return false;
	        }
	        else if (arg.equalsIgnoreCase("-server"))
	        { /* should be followed by 1 parameter for notification server url */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	serverName = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productid"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	                code = args[++ii];
	    			if(code.length() != 38)
	    			{
	    	            System.out.println("Invalid product code specified: " + code);
	    	            return false;
	    			}
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productver"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	version = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productlang"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	language = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productplat"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	platform = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-group"))
	        { /* should be followed by 1 parameter  */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	group1 = args[++ii];
	            	customInstEvent = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-action"))
	        { /* should be followed by 1 parameter */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	action1 = args[++ii];
	            	customInstEvent = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-value"))
	        { /* should be followed by 1 parameter */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	value1 = args[++ii];
	            	customInstEvent = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-dictionaryname"))
	        { /* should be followed by 1 parameter */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	dictName1 = args[++ii];
	            	customDictData = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-dictionaryvalue"))
	        { /* should be followed by 1 parameter */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	dictValueText = args[++ii];
	            	customDictData = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-instance"))
	        { /* should be followed by 1 parameter */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	instanceIdentifier = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-maxlog"))
	        { /* should be followed by 1 parameter */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	maxLogSize = Long.parseLong(args[++ii]);
	            }
	        }
	        else if (arg.equalsIgnoreCase("-maxfrag"))
	        { /* should be followed by 1 parameter */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	maxUploadPacketSize = Integer.parseInt((args[++ii]));
	            }
	        }
	        else if (arg.equalsIgnoreCase("-test"))
	        { 
	        	test = true;
	        }
	        else if (arg.equalsIgnoreCase("-upload"))
	        { 
	        	upload = true;
	        }
	        else if (arg.equalsIgnoreCase("-purge"))
	        { 
	        	purge = true;
	        }
	        else if (arg.equalsIgnoreCase("-unregister"))
	        { 
	        	unregister = true;
	        }
	        else if (arg.equalsIgnoreCase("-eventupload"))
	        { 
	        	eventUpload = true;
	        }
	        else
	        {
	            invalidSpec = true;
	            unknownArg = true;
	        }
	    }

	    if (invalidSpec)
	    {
	        if (unknownArg)
	        {
	            System.out.println("unknown option: " + arg);
	        }
	        else
	        {
	            System.out.println("invalid specification for option: " + arg);
	        }
	        
	        usage();
	        return false;
	    }

	    return true;
	}
	
	
	private static void usage()
	{
		System.out.println("This example will log two instrumentation events for a product.  " +
				"\nIt will then upload all available events if the 'upload' option is selected. ");
		System.out.println();
		System.out.println("Instrumentation" + " [-server url] [-productid product_id] [-productver product_version] [-productlang product_language] [-productplat product_platform]");
		System.out.println("                [-group group] [-name name] [-value value] [-dictionaryname dictionaryname] [-dictionaryvalue dictionaryvalue]");
		System.out.println();
		System.out.println("-server             Url of the FlexNet Connect notification server.");
		System.out.println("                    Default is " + serverName);
		System.out.println("-productid          FlexNet Connect id of product to upgrade.");
		System.out.println("                    Default is " + code);
		System.out.println("-productver         FlexNet Connect version of product to upgrade.");
		System.out.println("                    Default is " + version);
		System.out.println("-productlang        FlexNet Connect language of product to upgrade.");
		System.out.println("                    Default is " + language);
		System.out.println("-productplat        FlexNet Connect product's platform.");
		System.out.println("                    Default is " + platform);
		System.out.println("-group              Instrumentation event group.");
		System.out.println("                    Default is " + group1);
		System.out.println("-action             Instrumentation event action.");
		System.out.println("                    Default is " + action1);
		System.out.println("-value              Instrumentation event value.");
		System.out.println("                    Default is " + value1);
		System.out.println("-dictionaryname     Instrumentation event value.");
		System.out.println("                    Default is " + dictName1);
		System.out.println("-dictionaryvalue    Instrumentation event value.");
		System.out.println("                    Default is " + dictValueText);
		System.out.println("-instance           Instrumentation long instance name.");
		System.out.println("                    Writes to the default log if none provided.");
		System.out.println("-maxlog             Maximum Instrumentation long size in bytes.");
		System.out.println("                    Default is " + maxLogSize);
		System.out.println("-maxfrag            Maximum log upload fragment size in bytes.");
		System.out.println("                    Default is " + maxUploadPacketSize);
		System.out.println("-test               Runs a series of FlexNet Instrumentation tests.");
		System.out.println("-upload             Uploads Instrumentation events created during tests.");
		System.out.println("-eventupload        Uploads an individual event created during tests.");
		System.out.println("-purge              Purges all Instrumentation logs.");
		System.out.println("-unregister         Unregisters the product after all tests are completed.");
		System.out.println();
	}

	private static void printProductInfo ()
	{
		System.out.println();
		System.out.println ("FlexNet Connect has successfully registered the following product - ");
		System.out.println ("Product Code  : " + code);
		System.out.println ("Version       : " + version);
		System.out.println ("Language      : " + language);
		System.out.println ("Platform      : " + platform);
		System.out.println ("Server        : " + serverName);
		System.out.println ("Data location : " + storageLocation);
		System.out.println();
	}
	
	private static void printException (FlxException e)
	{
		System.out.println("ERROR!");
		System.out.println(e.getMessage());
		
		if (e.getArguments() != null)
		{
			Object exceptionArgs[] = new Object[e.getArguments().length];
	        exceptionArgs = e.getArguments();
	        for (Object arg: exceptionArgs)
	        {
	            System.out.println(arg.toString());
	        }
		}
        
		System.out.println("Stacktrace - ");
		e.printStackTrace();
	}
}
