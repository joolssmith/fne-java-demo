/****************************************************************************
  Copyright (c) 2014-2018 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/

package connectexamples;

import java.util.Iterator;
import java.util.List;

import com.flexnet.connect.client.interfaces.ConnectFactory;
import com.flexnet.connect.client.interfaces.IConnect;
import com.flexnet.connect.client.interfaces.IDownload;
import com.flexnet.connect.client.interfaces.IDownloadUpdateStatusCallback;
import com.flexnet.connect.client.interfaces.IDownloadUpdateStatusUserdata;
import com.flexnet.connect.client.interfaces.INotification;
import com.flexnet.connect.client.interfaces.INotificationMessage;
import com.flexnet.connect.client.interfaces.INotificationUpdate;
import com.flexnet.connect.client.interfaces.IProduct;
import com.flexnet.connect.client.interfaces.ProductFactory;
import com.flexnet.connect.client.interfaces.IDownloadUpdateStatusUserdata.DownloadUpdateStage;
import com.flexnet.lm.FlxException;

public class Notification {

	/* FlexNet Connect Notification Server */
	private static String	serverName = "http://updatesuat.flexnetoperations.com";
	
	/* Product-related info */
	private static String	code = "{77777777-7777-7777-7777-777777777777}";
	private static String	version="1.0", language="1033", platform="WIN64";
	
	/* Test environment */
	private static String	storageLocation = System.getProperty("user.home");
	
	/* Update Status callback stuff */
	private static Notification testNotifObject = new Notification();
	private static IDownloadUpdateStatusUserdata 	updateStatusUserdata = testNotifObject.new DownloadUpdateStatusUserdata();
	private static IDownloadUpdateStatusCallback 	updateStatusCallback = testNotifObject.new DownloadUpdateStatusCallback();
	
	/* Pause percent */
	private static int userDefinedPauseAtPercentage = 0;
	
	/* Flags */
	private static boolean download = false, execute = false, unregister = false;
	
	public static void main(String[] args) 
	{
		/* The Connect object and Product object(s) */
		IConnect	connect = null;
		IProduct	product1 = null;
		
		/* Data needed for creating a Connect object */
		String 		customHostId = "1234567890";
		
		boolean		success = true;
		
		/* Validate command-line arguments */
		if(!validateArgs(args)) return;
		
		try
		{
			/* Use the ConnectFactory class to create a Connect object. */
			connect = ConnectFactory.connectCreate(IdentityClient.IDENTITY_DATA, storageLocation, customHostId);
			
			/* Before creating and registering a product, check if it has already been registered
			 * (using getProductReference) and if so, return the registered product.  Else, register the product. */
			
			if (connect.isProductRegistered(code)) {
				product1 = connect.getRegisteredProduct(code);
			}
			else {
				/* Use the ProductFactory class to create a Product object and register it with the Connect object */
				product1 = ProductFactory.productCreate(connect, code, version, language, platform, serverName);
				
				/* Register the product */
				connect.registerProduct(product1);
			}
			
			/* Print status */
			printProductInfo();
			
			/* Set the product's notification server */
			product1.setNotificationServer(serverName);
			
			/* Run Notification examples */
			runNotificationExamples(connect, product1);
		}
		catch (FlxException e)
		{
			printException(e);
			success = false;
		}
		
		if (unregister)
		{
			/* Unregister the product */
			System.out.println ("Unregistering the product... ");
			try 
			{
				connect.unregisterProduct(product1);
				System.out.print("Done.");
				System.out.println();
			} 
			catch (FlxException e) 
			{
				printException(e);
				success = false;
			}
		}
		
		if (success)
		{
			System.out.println();
			System.out.println ("Successfully completed all FlexNet Connect 'Notification' demonstrations.");
		}
		
		return;
	}

	private static void runNotificationExamples (IConnect connect, IProduct product) throws FlxException
	{
		List <INotification>						notificationCollection;
		INotification 								notificationItem;
		
		System.out.println ("Checking for updates / messages...");
			
		/* Retrieve notifications */
		notificationCollection = product.getNotifications();
		
		int size = notificationCollection.size();
		
		if (size == 0)
		{
			System.out.println ("No Notifications currently available for this product.");
		}
		else
		{
			System.out.println ("Retrieved " + size + " Notifications.");
		}
		System.out.println();
		
		int count = 1;
		
		for (Iterator<INotification> colIterator = notificationCollection.iterator();  colIterator.hasNext();)
		{
			/* Get the notification item */
			notificationItem = colIterator.next();
				
			/* Display the notification item */
			System.out.println ("Notification item " + count + " of " + size + " attributes:");
			displayMessageNotification(notificationItem);
			count++;
				
			/* If it is an update, download the update */
			if ((notificationItem instanceof INotificationUpdate) && download)
			{
				System.out.println ("Downloading the file...");
				((DownloadUpdateStatusUserdata)updateStatusUserdata).lastPercentComplete = 0;
				((DownloadUpdateStatusUserdata)updateStatusUserdata).lastUpdateStage = DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_IDLE;
				downloadUpdateNotification((INotificationUpdate)notificationItem, updateStatusUserdata, updateStatusCallback);
			}
		}
		
		return;
	}
	
	
	private static void displayMessageNotification (INotification notification)
	{
		/* Display each Notification Item property */
		
		System.out.println();
		
		if (notification instanceof INotificationMessage) System.out.println("Type                : Message");
		else System.out.println("Type                : Update");
		
		System.out.println("Title               : " + notification.getTitle());
		System.out.println("Product Code        : " + notification.getProductCode());
		System.out.println("Product Version     : " + notification.getProductVersion());
		System.out.println("Notification ID     : " + notification.getId());
		System.out.println("Product Name        : " + notification.getProductName());
		System.out.println("Info Url            : " + notification.getInfoUrl());
		System.out.println("Description         : " + notification.getDescription());
		System.out.println("Start Date          : " + notification.getStartDate());
		System.out.println("Expiration Date     : " + notification.getExpireDate());
		System.out.println("Hidden              : " + notification.getHidden());
		System.out.println("Display Name        : " + notification.getDisplayName());
		System.out.println("Company Logo        : " + notification.getCompanyLogo());
		
		if (notification instanceof INotificationUpdate)
		{
			System.out.println("Download Url        : " + notification.getDownloadUrl());
			System.out.println("Download Size       : " + notification.getDownloadSize());
			System.out.println("Command Line        : " + notification.getCommandLine());
			System.out.println("Local Directory     : " + notification.getLocalDir());	
			System.out.println("Category            : " + notification.getCategory());
			System.out.println("Package Type        : " + notification.getPackageType());
			System.out.println("Install Inst        : " + notification.getInstallInstructions());
			System.out.println("Type                : " + notification.getType());
			System.out.println("Callout Url         : " + notification.getCalloutUrl());
			System.out.println("Security Type       : " + notification.getSecurityType());
			System.out.println("Security Signature  : " + notification.getSecuritySignature());
			if (notification.getPackageType() == IDownload.NotificationPackageType.NOTIFICATION_PKGTYPE_FILESYNC.ordinal())
				System.out.println("Secondary Url       : " + notification.getSecondaryUrl());
			System.out.println("Download Type       : " + notification.getDownloadType());
			System.out.println("Elevation Required  : " + notification.getElevationRequired());
			System.out.println("Product Logo        : " + notification.getProductLogo());
			System.out.println("Release Id          : " + notification.getReleaseId());
		}
		
		System.out.println();
		
	}
	
	
	private static void downloadUpdateNotification (INotificationUpdate notification, 
			IDownloadUpdateStatusUserdata 	updateStatusUserdata, IDownloadUpdateStatusCallback 	updateStatusCallback)
	{
		String					targetFolderPath = null;
		String					targetFileName = null;
		String					downloadedFile = null;
		IDownload				notifItem;
		
		
		/* Cast 'notification' to IDownload so the download methods can be called */
		notifItem = (IDownload)notification;
			
		try
		{
			if (execute)
			{
				/* Download & execute. */
				downloadedFile = notifItem.downloadAndExecute(targetFolderPath, targetFileName, updateStatusCallback, updateStatusUserdata);	
			}
			else
			{
				/* Simply download the file */
				downloadedFile = notifItem.download(targetFolderPath, targetFileName, updateStatusCallback, updateStatusUserdata);
			}
			
			if (notifItem.isDownloadPaused())
			{
				/* Resume the download */
				System.out.println("Resuming paused download...");
				downloadedFile = notifItem.resumeDownload();
			}
			
			if (notifItem.isDownloadPaused())
			{
				System.out.println("Download of Notification #" + notification.getId() + " is paused.");
			}
			else 
			{
				if (notification.getPackageType() == IDownload.NotificationPackageType.NOTIFICATION_PKGTYPE_FILESYNC.ordinal())
				{
					System.out.println("File " + notification.getDownloadUrl() + " has been synchronized on the client.");
				}
				else
				{
					if (downloadedFile != null)
					{
						/* Console message */
						System.out.println("File " + downloadedFile + " downloaded.");
					}
				}
			}
		}
		catch (FlxException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return;
	}

	
	private static boolean validateArgs(String[] args)
	{
	    boolean     invalidSpec = false;
	    boolean     unknownArg  = false;
	    int    		ii = 0, numArgs = 0;;
	    String	    arg = null;
	    
	    numArgs = args.length;

	    for (ii = 0; !invalidSpec && ii < numArgs; ii++)
	    {
	    	arg = args[ii];
	        if (arg.equalsIgnoreCase("-help") || arg.equalsIgnoreCase("-h"))
	        {
	            usage();
	            return false;
	        }
	        else if (arg.equalsIgnoreCase("-server"))
	        { /* should be followed by 1 parameter for notification server url */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	serverName = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productid"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	                code = args[++ii];
	    			if(code.length() != 38)
	    			{
	    	            System.out.println("Invalid product code specified: " + code);
	    	            return false;
	    			}
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productver"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	version = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productlang"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	language = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productplat"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	platform = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-download"))
	        { 
	        	download = true;
	        }
	        else if (arg.equalsIgnoreCase("-execute"))
	        { 
	        	execute = true;
	        }	        
	        else if (arg.equalsIgnoreCase("-unregister"))
	        { 
	        	unregister = true;
	        }
	        else
	        {
	            invalidSpec = true;
	            unknownArg = true;
	        }
	    }

	    if (invalidSpec)
	    {
	        if (unknownArg)
	        {
	            System.out.println("unknown option: " + arg);
	        }
	        else
	        {
	            System.out.println("invalid specification for option: " + arg);
	        }
	        
	        usage();
	        return false;
	    }

	    return true;
	}
	
	
	private static void usage()
	{
		System.out.println("This example will download any available Notifications for the product.  " +
						 "\nIt can then download the payload indicatated by the Notification " +
						 "\nand also proceed to execute the downloaded file.");
		System.out.println();
		System.out.println("Notification" + " [-server url] [-productid product_id] [-productver product_version] [-productlang product_language] [-productplat product_platform]");
		System.out.println();
		System.out.println("-server      Url of the FlexNet Connect notification server.");
		System.out.println("             Default is " + serverName);
		System.out.println("-productid   FlexNet Connect id of product to upgrade.");
		System.out.println("             Default is " + code);
		System.out.println("-productver  FlexNet Connect version of product to upgrade.");
		System.out.println("             Default is " + version);
		System.out.println("-productlang FlexNet Connect language of product to upgrade.");
		System.out.println("             Default is " + language);
		System.out.println("-productplat FlexNet Connect product's platform.");
		System.out.println("             Default is " + platform);
		System.out.println("-download    FlexNet Connect will download any available update(s).");
		System.out.println("-execute     FlexNet Connect will execute any downloaded updates.");
		System.out.println("-unregister  Unregisters the product after all tests are completed.");
		System.out.println();
	}

	private static void printProductInfo ()
	{
		System.out.println();
		System.out.println ("FlexNet Connect has successfully registered the following product - ");
		System.out.println ("Product Code  : " + code);
		System.out.println ("Version       : " + version);
		System.out.println ("Language      : " + language);
		System.out.println ("Platform      : " + platform);
		System.out.println ("Server        : " + serverName);
		System.out.println ("Data location : " + storageLocation);
		System.out.println();
	}
	
	private class DownloadUpdateStatusCallback implements IDownloadUpdateStatusCallback{

		public DownloadUpdateStatusCallbackReturnType downloadUpdateStatusCallbackMethod(DownloadUpdateStage stage, 
				long currentSize, long totalSize, IDownloadUpdateStatusUserdata userData)
		{
			/* Do whatever you want here to display progress */
			
			long percentComplete = 0;
			
			/* Display change in state */
			if (stage != ((DownloadUpdateStatusUserdata)userData).lastUpdateStage)
			{
				((DownloadUpdateStatusUserdata)userData).lastUpdateStage = stage;
				if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_DOWNLOAD_START) System.out.println("Download started.");
				else if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_DOWNLOAD_END)
				{
					System.out.println();
					System.out.println("Download ended.");
				}
				else if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_EXECUTION_START) System.out.println("Execution started.");
				else if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_EXECUTION_END) System.out.println("Execution ended.");
			}
			
			if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_DOWNLOADING)
			{
				if (totalSize > 0)
				{
					percentComplete = (long)(((double)((double)currentSize / (double)totalSize)) * 100);
					if (percentComplete > ((DownloadUpdateStatusUserdata)userData).lastPercentComplete)
					{
						int 					progressBarWidth = 50;
						int 					progressBarPos = 0;	
						
						progressBarPos = (int)(progressBarWidth * ((double)currentSize / (double)totalSize));
						System.out.print("\r");
						System.out.print("[");
						for (int i = 0; i < progressBarWidth; ++i) 
						{
							if (i < progressBarPos) System.out.print("=");
							else if (i == progressBarPos) System.out.print(">");
							else System.out.print(" ");
						}
						if (percentComplete >= 100) System.out.print("]");
						System.out.print(" " + percentComplete + " %");
						
						((DownloadUpdateStatusUserdata)userData).lastPercentComplete = percentComplete;
					}
				}
				else
				{
					System.out.print("\r");
					System.out.print("Downloading.  Please wait...");
				}
			}
			
			DownloadUpdateStatusCallbackReturnType	callbackReturnValue = DownloadUpdateStatusCallbackReturnType.FLC_UPDATE_STATUS_RETURN_CONTINUE;
			
			if ((userDefinedPauseAtPercentage > 0) && (percentComplete >= userDefinedPauseAtPercentage))
			{
				/* Simulating a simple scenario to test 'Pause' */
				System.out.println ("Download paused at " + userDefinedPauseAtPercentage + " percent.");
				callbackReturnValue = DownloadUpdateStatusCallbackReturnType.FLC_UPDATE_STATUS_RETURN_PAUSE;
				userDefinedPauseAtPercentage = 0;
			}
			else if ((totalSize > 0) && (currentSize > totalSize))
			{
				/* Something's odd.  If you wish to cancel the download, uncomment the line below.*/
				// callbackReturnValue = DownloadUpdateStatusCallbackReturnType.FLC_UPDATE_STATUS_RETURN_CANCEL;
			}
			
			return callbackReturnValue;
		}
	}
	
	private class DownloadUpdateStatusUserdata implements IDownloadUpdateStatusUserdata{

		/* Customize the user data object you'd like passed through the Update Status callback
		 * mechanism to suit your needs. */
		
		public long lastPercentComplete;
		public DownloadUpdateStage lastUpdateStage;

		public DownloadUpdateStatusUserdata()
		{
			lastPercentComplete = 0;
			lastUpdateStage = DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_IDLE;
		}
	}

	private static void printException (FlxException e)
	{
		System.out.println("ERROR!");
		System.out.println(e.getMessage());
		
		if (e.getArguments() != null)
		{
			Object exceptionArgs[] = new Object[e.getArguments().length];
	        exceptionArgs = e.getArguments();
	        for (Object arg: exceptionArgs)
	        {
	            System.out.println(arg.toString());
	        }
		}
        
		System.out.println("Stacktrace - ");
		e.printStackTrace();
	}
}
