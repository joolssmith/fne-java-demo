/****************************************************************************
  Copyright (c) 2014-2018 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/

package connectexamples;

import com.flexnet.connect.client.interfaces.ConnectFactory;
import com.flexnet.connect.client.interfaces.IConnect;
import com.flexnet.connect.client.interfaces.IProduct;
import com.flexnet.connect.client.interfaces.IProfile;
import com.flexnet.connect.client.interfaces.ProductFactory;
import com.flexnet.connect.client.interfaces.exceptions.IBadParameterException;
import com.flexnet.lm.FlxException;

public class Profile {
	
	/* FlexNet Connect Notification Server */
	private static String	serverName = "http://updatesuat.flexnetoperations.com";
	
	/* Product-related info */
	private static String	code = "{77777777-7777-7777-7777-777777777777}";
	private static String	version="1.0", language="1033", platform="WIN64";
	private static String	storageLocation = System.getProperty("user.home");
	private static IProduct	product1 = null;
	
	private static String	group = "Test Group A", name = "Test Name 1", value = "Test Value 1";
	
	/* Test flags */
	private static boolean test = false;
	private static boolean unregister = false;
	private static boolean customProfileItem = false;
	
	public static void main(String[] args) 
	{
		/* The Connect object and Product object(s) */
		IConnect	connect = null;
		
		/* Data needed for creating a Connect object */
		String customHostId = "1234567890";
		
		boolean		success = true;
		
		/* Validate command-line arguments */
		if(!validateArgs(args)) return;
		
		try
		{
			/* Use the ConnectFactory class to create a Connect object. */
			connect = ConnectFactory.connectCreate(IdentityClient.IDENTITY_DATA, storageLocation, customHostId);
			
			/* Before creating and registering a product, check if it has already been registered
			 * (using getProductReference) and if so, return the registered product.  Else, register the product. */
			
			if (connect.isProductRegistered(code)) {
				product1 = connect.getRegisteredProduct(code);
			}
			else {
				/* Use the ProductFactory class to create a Product object and register it with the Connect object */
				product1 = ProductFactory.productCreate(connect, code, version, language, platform, serverName);
				
				/* Register the product */
				connect.registerProduct(product1);
			}
			
			/* Print status */
			printProductInfo();
			
			/* Set the product's notification server */
			product1.setNotificationServer(serverName);
			
			/* Run Profile examples */
			if (test) runProfileExamples(connect, product1);
			
			/* Process the custom Profile test request */
			if (customProfileItem) testCustomProfile (connect, product1);
		}
		catch (FlxException e)
		{
			printException(e);
			success = false;
		}
		
		if (unregister)
		{
			/* Unregister the product */
			System.out.println ("Unregistering the product... ");
			try 
			{
				connect.unregisterProduct(product1);
				System.out.print("Done.");
				System.out.println();
			} 
			catch (FlxException e) 
			{
				printException(e);
				success = false;
			}
		}
		
		if (success)
		{
			System.out.println();
			System.out.println ("Successfully completed all FlexNet Connect 'Profile' demonstrations.");
		}
		
		return;
	}

	private static void runProfileExamples (IConnect connect, IProduct product)
	{
		String	name1a = "Test Name 1a", value1a = "Test Value 1a";
		String	group2 = "Test Group B", name2 = "Test Name 2", value2 = "Test Value 2",
				name2a = "Test Name 2a", value2a = "Test Value 2a";
		String	retrievedValue = null;
		boolean	demographicsAllow;
		IProfile profile;
		
		try
		{
			/* Create/retrieve a Product Profile */
			profile = product.getProfile();
			
			/* Add / modify an attribute */
			System.out.println("Setting Profile item - Group: " + group + " Name: " + name + " Value: " + value);
			profile.set(group, name, value);
			
			profile.set(group, name, value);
			System.out.println("Setting Profile item - Group: " + group + " Name: " + name1a + " Value: " + value1a);
			profile.set(group, name1a, value1a);
			System.out.println("Setting Profile item - Group: " + group2 + " Name: " + name2 + " Value: " + value2);
			profile.set(group2, name2, value2);
			System.out.println("Setting Profile item - Group: " + group2 + " Name: " + name2a + " Value: " + value2a);
			profile.set(group2, name2a, value2a);
			System.out.println();
		
			/* Get an attribute */
			retrievedValue = profile.get(group, name);
			System.out.println("Retrieved Profile item - Group: " + group + " Name: " + name + " Value: " + retrievedValue);
			retrievedValue = profile.get(group2, name2);
			System.out.println("Retrieved Profile item - Group: " + group2 + " Name: " + name2 + " Value: " + retrievedValue);
			retrievedValue = profile.get(group, "badName");
			System.out.println("Retrieved Profile item - Group: " + group + " Name: " + "badName" + " Value: " + retrievedValue);
			System.out.println();
			
			/* Set Demographics 'allow'  */
			System.out.println ("Setting the 'Demographics Allowed' flag to false");
			profile.setDemographicsAllowed(false);
			System.out.println();
			
			/* Get Demographics 'allow'  */
			demographicsAllow = profile.isDemographicsAllowed();
			System.out.println("Retrieved 'Demographics Allow' state: " + demographicsAllow);
			System.out.println();
			
			/* Set it back to 'true' */
			System.out.println ("Setting the 'Demographics Allowed' flag to true");
			profile.setDemographicsAllowed(true);
			System.out.println();
			
			/* Delete an attribute */
			System.out.println("Removing Profile item - Group: " + group + " Name: " + name );
			profile.removeItem(group, name);
			System.out.println();
			
			/* Delete an attribute that does not exist - expect a IBadParameterException exception */
			try
			{
				System.out.println ("Attempting to remove an item that does not exist... ");
				profile.removeItem(group, "badName");
			}
			catch (FlxException e)
			{
				System.out.print("Expected Exception Caught!");
				if (!(e instanceof IBadParameterException)) throw new FlxException (e);
				System.out.println();
				System.out.println();
			}
			
			/* Remove the one remaining item from this group.  This should delete the entire group. */
			System.out.println("Revoving Profile item - Group: " + group + " Name: " + name1a );
			profile.removeItem(group, name1a);
		
			/* Delete a group that does not exist - expect a IBadParameterException exception */
			try
			{
				System.out.println ("Attempting to remove a group that does not exist... ");
				profile.removeGroup("badGroup");
			}
			catch (FlxException e)
			{
				System.out.print("Expected Exception Caught!");
				if (!(e instanceof IBadParameterException)) throw new FlxException (e);
				System.out.println();
				System.out.println();
			}
			
			/* Delete a group */
			System.out.println("Removing Profile group: " + group2);
			profile.removeGroup(group2);
			System.out.println();
			
			/* Add / modify an attribute */
			System.out.println("Setting Profile item - Group: " + group + " Name: " + name + " Value: " + value);
			profile.set(group, name, value);
			
			/* Upload the profile */
			upload(true);
		}
		catch (FlxException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return;
	}
	
	private static void testCustomProfile (IConnect connect, IProduct product)
	{
		IProfile profile;
		
		try
		{
			/* Create/retrieve a Product Profile */
			profile = product.getProfile();
			
			/* Set */
			System.out.println("Setting Profile item - Group: " + group + " Name: " + name + " Value: " + value);
			profile.set(group, name, value);
			
			/* Upload the profile */
			upload(true);
		}
		catch (FlxException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return;
	}
	
	private static void upload (boolean uploadAll) throws FlxException
	{
		
		/* Upload Profile data  */
		System.out.println("Uploading Profile data to server... ");
		product1.getProfile().upload(uploadAll);
		System.out.print("Done!");
		System.out.println();
		System.out.println();
		
		return;
	}
	
	private static boolean validateArgs(String[] args)
	{
	    boolean     invalidSpec = false;
	    boolean     unknownArg  = false;
	    int    		ii = 0, numArgs = 0;;
	    String	    arg = null;
	    
	    numArgs = args.length;

	    for (ii = 0; !invalidSpec && ii < numArgs; ii++)
	    {
	        arg = args[ii];
	        if (arg.equalsIgnoreCase("-help") || arg.equalsIgnoreCase("-h"))
	        {
	            usage();
	            return false;
	        }
	        else if (arg.equalsIgnoreCase("-server"))
	        { /* should be followed by 1 parameter for notification server url */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	serverName = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productid"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	                code = args[++ii];
	    			if(code.length() != 38)
	    			{
	    	            System.out.println("Invalid product code specified: " + code);
	    	            return false;
	    			}
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productver"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	version = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productlang"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	language = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productplat"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	platform = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-group"))
	        { /* should be followed by 1 parameter*/
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	group = args[++ii];
	            	customProfileItem = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-name"))
	        { /* should be followed by 1 parameter*/
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	name = args[++ii];
	            	customProfileItem = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-value"))
	        { /* should be followed by 1 parameter*/
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	value = args[++ii];
	            	customProfileItem = true;
	            }
	        }
	        else if (arg.equalsIgnoreCase("-test"))
	        { 
	        	test = true;
	        }
	        else if (arg.equalsIgnoreCase("-unregister"))
	        { 
	        	unregister = true;
	        }
	        else
	        {
	            invalidSpec = true;
	            unknownArg = true;
	        }
	    }

	    if (invalidSpec)
	    {
	        if (unknownArg)
	        {
	            System.out.println("unknown option: " + arg);
	        }
	        else
	        {
	            System.out.println("invalid specification for option: " + arg);
	        }
	        
	        usage();
	        return false;
	    }

	    return true;
	}
	
	
	private static void usage()
	{
		System.out.println("This example will perform varios operations on the Product's profile data." +
				"\nIt will also upload this data to the server if the 'test' option is selected.");
		System.out.println();
		System.out.println("Profile" + " [-server url] [-productid product_id] [-productver product_version] [-productlang product_language] [-productplat product_platform]");
		System.out.println("        [-group group] [-name name] [-value value]");
		System.out.println();
		System.out.println("-server      Url of the FlexNet Connect notification server.");
		System.out.println("             Default is " + serverName);
		System.out.println("-productid   FlexNet Connect id of product to upgrade.");
		System.out.println("             Default is " + code);
		System.out.println("-productver  FlexNet Connect version of product to upgrade.");
		System.out.println("             Default is " + version);
		System.out.println("-productlang FlexNet Connect language of product to upgrade.");
		System.out.println("             Default is " + language);
		System.out.println("-productplat FlexNet Connect product's platform.");
		System.out.println("             Default is " + platform);
		System.out.println("-group       Test Profile data item group.");
		System.out.println("             Default is " + group);
		System.out.println("-name        Test Profile data item name.");
		System.out.println("             Default is " + name);
		System.out.println("-value       Test Profile data item value.");
		System.out.println("             Default is " + value);
		System.out.println("-test        FlexNet Connect will perform various Profile-related tests.");
		System.out.println("-unregister  Unregisters the product after all tests are completed.");
		System.out.println();
	}

	private static void printProductInfo ()
	{
		System.out.println();
		System.out.println ("FlexNet Connect has successfully registered the following product - ");
		System.out.println ("Product Code  : " + code);
		System.out.println ("Version       : " + version);
		System.out.println ("Language      : " + language);
		System.out.println ("Platform      : " + platform);
		System.out.println ("Server        : " + serverName);
		System.out.println ("Data location : " + storageLocation);
		System.out.println();
	}
	
	private static void printException (FlxException e)
	{
		System.out.println("ERROR!");
		System.out.println(e.getMessage());
		
		if (e.getArguments() != null)
		{
			Object exceptionArgs[] = new Object[e.getArguments().length];
	        exceptionArgs = e.getArguments();
	        for (Object arg: exceptionArgs)
	        {
	            System.out.println(arg.toString());
	        }
		}
        
		System.out.println("Stacktrace - ");
		e.printStackTrace();
	}
}
