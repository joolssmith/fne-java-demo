/****************************************************************************
  Copyright (c) 2014-2018 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/

package connectexamples;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import com.flexnet.connect.client.interfaces.ConnectFactory;
import com.flexnet.connect.client.interfaces.IConnect;
import com.flexnet.connect.client.interfaces.IDownload;
import com.flexnet.connect.client.interfaces.IDownloadUpdateStatusCallback;
import com.flexnet.connect.client.interfaces.IDownloadUpdateStatusUserdata;
import com.flexnet.connect.client.interfaces.INotification;
import com.flexnet.connect.client.interfaces.INotificationUpdate;
import com.flexnet.connect.client.interfaces.IProduct;
import com.flexnet.connect.client.interfaces.ProductFactory;
import com.flexnet.connect.client.interfaces.IDownloadUpdateStatusUserdata.DownloadUpdateStage;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;

public class SoftwareUpgrade {
	
	/****************************************************************************
    	SoftwareUpgrade.java

    	This example program enables you to:
    	1. Process the license file of your choice.
    	2. Check for product updates.
    	3. Determine whether the product update is for a licensed feature.
    	4. If a license exists, download the product update.
    	
	 *****************************************************************************/
	
	/* Local License File on client */
	private static String	licenseFile = "license.bin";
	
	/* FlexNet Connect Notification Server */
	private static String	serverName = "http://updatesuat.flexnetoperations.com";
	
	/* Product-related info */
	private static String	productName = "MyProduct";
	private static String	code = "{77777777-7777-7777-7777-777777777777}";
	private static String	version="1.0", language="1033", platform="WIN64";
	
	/* Test environment */
	private static String	storageLocation = System.getProperty("user.home");
	
	/* Update Status callback stuff */
	private static SoftwareUpgrade testSoftwareUpgrade = new SoftwareUpgrade();
	private static IDownloadUpdateStatusUserdata 	updateStatusUserdata = testSoftwareUpgrade.new DownloadUpdateStatusUserdata();
	private static IDownloadUpdateStatusCallback 	updateStatusCallback = testSoftwareUpgrade.new DownloadUpdateStatusCallback();
	
	/* Pause percent */
	private static int userDefinedPauseAtPercentage = 0;
	
	private static boolean unregister = false, execute = false;
	
	public static void main(String[] args) 
	{
		/* The Connect object and Product object(s) */
		IConnect	connect = null;
		IProduct	product1 = null;
		
		/* Licensing objects */
		ILicensing		licensing = null;
		ILicenseManager	licenseManager = null;
		String			licensingName = "softwareupgrade";
		
		/* Data needed for creating a Connect object */
		String 		customHostId = "1234567890";
		
		boolean		success = true;
		
		/* Validate command-line arguments */
		if(!validateArgs(args)) return;
		
		try
		{
			/* Initialize ILicensing interface with identity data using file-based trusted storage in
			   user's home directory and hard-coded string hostid "1234567890" */
			licensing = LicensingFactory.getLicensing(IdentityClient.IDENTITY_DATA, storageLocation, customHostId, licensingName);

			/* Get ILicenseManager interface, the primary object for licensing-related functionality */
			licenseManager = licensing.getLicenseManager();
			
			/* Add license sources */
			try
			{
				System.out.println("Reading data from " + licenseFile + ".");
				licenseManager.addBufferLicenseSource(BinaryMessage.readData(licenseFile));
			}
			catch (FlxException e)
			{
				System.out.println("No buffered license source available.");
			}
			licenseManager.addTrialLicenseSource();
			licenseManager.addTrustedStorageLicenseSource();
			
			/* Report the feature count */
			reportFeatureCount (licenseManager);
			
			/* Use the ConnectFactory class to create a Connect object. */
			connect = ConnectFactory.connectCreate(IdentityClient.IDENTITY_DATA, storageLocation, customHostId);
			
			/* Before creating and registering a product, check if it has already been registered
			 * (using getProductReference) and if so, return the registered product.  Else, register the product. */
			
			if (connect.isProductRegistered(code)) {
				product1 = connect.getRegisteredProduct(code);
			}
			else {
				/* Use the ProductFactory class to create a Product object and register it with the Connect object */
				product1 = ProductFactory.productCreate(connect, code, version, language, platform, serverName);
				
				/* Register the product */
				connect.registerProduct(product1);
			}
			
			/* Print status */
			printProductInfo();
			
			/* Set the product's notification server */
			product1.setNotificationServer(serverName);
			
			/* Run Notification examples */
			checkForUpdates(connect, product1, licenseManager, productName, version);
			
		}
		catch (FlxException e)
		{
			printException(e);
			success = false;
		}

		if (unregister)
		{
			/* Unregister the product */
			System.out.println ("Unregistering the product... ");
			try 
			{
				connect.unregisterProduct(product1);
				System.out.print("Done.");
				System.out.println();
			} 
			catch (FlxException e) 
			{
				printException(e);
				success = false;
			}
		}
		
		if (success)
		{
			System.out.println();
			System.out.println ("Successfully completed all FlexNet Connect 'Software Upgrade' demonstrations.");
		}
		
		return;
	}

	private static void reportFeatureCount (ILicenseManager licenseManager) throws FlxException
	{
		List<IFeature> featureList = null;
		int	numFeatures = 0;
		
		/* Buffered ... */
		try
		{
			featureList = licenseManager.getFeaturesFromBuffer(licenseFile, false);
			if (featureList != null) numFeatures = featureList.size();
		}
		catch (FlxException e)
		{
			/* Treat this as an bad buffer and move on.  The print statement
			 * will report that there were 0 features from the buffered license source.  */
		}
		System.out.println("Number of features from buffer : " + numFeatures);
		numFeatures = 0;
		
		/* Trusted storage ... */
		featureList = licenseManager.getFeaturesFromTrustedStorage(false);
		if (featureList != null) numFeatures = featureList.size();
		System.out.println("Number of features from Trusted Storage : " + numFeatures);
		numFeatures = 0;
		
		/* Trials ... */
		featureList = licenseManager.getFeaturesFromTrials();
		if (featureList != null) numFeatures = featureList.size();
		System.out.println("Number of features from Trials : " + numFeatures);
		
		return;
	}
	
	private static void acquireLicense(ILicenseManager licenseManager, String name, String version, int count) throws FlxException {
		ILicense	license = null;
		try {
			license = licenseManager.acquire(name,  version, count);
			System.out.println("Successfully acquired \"" + license.getName() + "\", version " + 
					license.getVersion() +  ", " + license.getCount() + " count.");
			try {
				// return license
				licenseManager.returnLicense(license);
			}
			catch (FlxException e) {
				throw new FlxException ("Unable to return " + name + ": " + e.getMessage());
			}
		}
		catch (FlxException e) {
			throw new FlxException ("Unable to acquire " + name + ": " + e.getMessage());
		}
	}
	
	private static void checkForUpdates (IConnect connect, IProduct product, ILicenseManager licenseManager,
			String productName, String version) throws FlxException
	{
		List <INotification>						notificationCollection;
		INotification 								notificationItem;
		
		System.out.println ("Checking for updates...");
		
		/* Retrieve notifications */
		notificationCollection = product.getNotifications();
		
		boolean updateAvailable = false;
		
		for (Iterator<INotification> colIterator = notificationCollection.iterator();  colIterator.hasNext();)
		{
			/* Get the notification item */
			notificationItem = colIterator.next();
				
			/* Is this an update? */
			if (notificationItem instanceof INotificationUpdate)
			{
				/* Now check the product name */
				if (notificationItem.getProductName().equals(productName))
				{
					updateAvailable = true;
					
					/* There is an update that is ready for installation.  
					 * See if we have a license. */
					System.out.println ("Upgrade available for this product.  Acquiring the necessary license(s)");
					acquireLicense(licenseManager, productName, version, 1);
						
					/* If the license was acquired, go ahead and download the udpate. */
					System.out.println ("License(s) acquired.  Downloading the upgrade.");
					((DownloadUpdateStatusUserdata)updateStatusUserdata).lastPercentComplete = 0;
					((DownloadUpdateStatusUserdata)updateStatusUserdata).lastUpdateStage = DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_IDLE;
					downloadUpdateNotification((INotificationUpdate)notificationItem, updateStatusUserdata, updateStatusCallback);
				}
			}
		}
		
		if (!updateAvailable)
		{
			System.out.println("No updates available for this product at this time.");
		}
		
		return;
	}
	
	
	private static void downloadUpdateNotification (INotificationUpdate notification, 
													IDownloadUpdateStatusUserdata updateStatusUserdata, 
													IDownloadUpdateStatusCallback updateStatusCallback) throws FlxException
	{
		String					targetFolderPath = storageLocation;
		String					targetFileName = null;
		String					downloadedFile = null;
		IDownload				notifItem;
		
		/* Cast 'notification' to IDownload so the download methods can be called */
		notifItem = (IDownload)notification;
			
		if (execute)
		{
			/* Download & execute. */
			downloadedFile = notifItem.downloadAndExecute(targetFolderPath, targetFileName, updateStatusCallback, updateStatusUserdata);	
		}
		else
		{
			/* Simply download the file */
			downloadedFile = notifItem.download(targetFolderPath, targetFileName, updateStatusCallback, updateStatusUserdata);
		}
			
			
		if (notifItem.isDownloadPaused())
		{
			/* Resume the download */
			System.out.println("Resuming paused download...");
			downloadedFile = notifItem.resumeDownload();
		}
		
		if (!(notifItem.isDownloadPaused()) )
		{
			if (notification.getPackageType() == IDownload.NotificationPackageType.NOTIFICATION_PKGTYPE_FILESYNC.ordinal())
			{
				System.out.println("File " + notification.getDownloadUrl() + " has been synchronized on the client.");
			}
			else
			{
				if (downloadedFile != null)
				{
					/* Console message */
					System.out.println("File " + downloadedFile + " downloaded.");
				}
			}
		}
		
		return;
	}
	
	
	private static boolean validateArgs(String[] args)
	{
	    boolean     invalidSpec = false;
	    boolean     unknownArg  = false;
	    int    		ii = 0, numArgs = 0;;
	    String	    arg = null;
	    
	    numArgs = args.length;

	    for (ii = 0; !invalidSpec && ii < numArgs; ii++)
	    {
	        arg = args[ii];
	        if (arg.equalsIgnoreCase("-help") || arg.equalsIgnoreCase("-h"))
	        {
	            usage();
	            return false;
	        }
	        else if (arg.equalsIgnoreCase("-licensefile"))
	        { /* should be followed by 1 parameter for input buffer license filename */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	licenseFile = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-server"))
	        { /* should be followed by 1 parameter for notification server url */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	serverName = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productid"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	                code = args[++ii];
	    			if(code.length() != 38)
	    			{
	    	            System.out.println("Invalid product code specified: " + code);
	    	            return false;
	    			}
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productname"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	productName = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productver"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	version = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productlang"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	language = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-productplat"))
	        { /* should be followed by 1 parameter for connect product id */
	            invalidSpec = (numArgs <= ii + 1) ? true : false;
	            if (!invalidSpec)
	            {
	            	platform = args[++ii];
	            }
	        }
	        else if (arg.equalsIgnoreCase("-unregister"))
	        { 
	        	unregister = true;
	        }
	        else if (arg.equalsIgnoreCase("-execute"))
	        { 
	        	execute = true;
	        }
	        else
	        {
	            invalidSpec = true;
	            unknownArg = true;
	        }
	    }

	    if (invalidSpec)
	    {
	        if (unknownArg)
	        {
	            System.out.println("unknown option: " + arg);
	        }
	        else
	        {
	            System.out.println("invalid specification for option: " + arg);
	        }
	        
	        usage();
	        return false;
	    }

	    return true;
	}
	
	
	private static void usage()
	{
		System.out.println("Selects appropriate available product updates based on product name and available licenses. ");
		System.out.println();
		System.out.println("SoftwareUpgrade" + " [-licensefile binary_license_file] [-server url] [-productid product_id] [-productname product_name] [-productver product_version]");
		System.out.println("                [-productlang product_language] [-productplat product_platform]");
		System.out.println();
		System.out.println("-licensefile   Name of a binary buffer license file to be used as a license source.");
		System.out.println("               Default is " + licenseFile);
		System.out.println("-server        Url of the FlexNet Connect notification server.");
		System.out.println("               Default is " + serverName);
		System.out.println("-productid     FlexNet Connect id of product to upgrade.");
		System.out.println("               Default is " + code);
		System.out.println("-productname   FlexNet Connect name of product to upgrade.");
		System.out.println("               Default is " + productName);
		System.out.println("-productver    FlexNet Connect version of product to upgrade.");
		System.out.println("               Default is " + version);
		System.out.println("-productlang   FlexNet Connect language of product to upgrade.");
		System.out.println("               Default is " + language);
		System.out.println("-productplat   FlexNet Connect product's platform.");
		System.out.println("               Default is " + platform);
		System.out.println("-unregister    Unregisters the product after all tests are completed.");
		System.out.println("-execute       FlexNet Connect will execute any downloaded updates.");
		System.out.println();
	}

	private static void printProductInfo ()
	{
		System.out.println();
		System.out.println ("FlexNet Connect has successfully registered the following product - ");
		System.out.println ("Product Code  : " + code);
		System.out.println ("Version       : " + version);
		System.out.println ("Language      : " + language);
		System.out.println ("Platform      : " + platform);
		System.out.println ("Product Name  : " + productName);
		System.out.println ("Server        : " + serverName);
		System.out.println ("License File  : " + licenseFile);
		System.out.println ("Data location : " + storageLocation);
		System.out.println();
	}
	
	private static class BinaryMessage {
		/**
		 * Read binary from file
		 * @param inputFile File path including the name to be read
		 * @return data as byte array
		 */
		public static byte[] readData(String inputFile) {
			
			byte[] responseData = null;
			try {
				FileInputStream idfile = new FileInputStream(inputFile);
				responseData = new byte[(int)(idfile.getChannel().size())];
				idfile.read(responseData);
				idfile.close();
			}
			catch (FileNotFoundException e) {
				System.out.println("Unable to find file " + inputFile + ".");
			}
			catch (IOException e) {						
				System.out.println("Error reading data from file " + inputFile + ".");
			}
			return responseData;
		}

		/**
		 * Write buffer to output file
		 * @param outputFile File path and name 
		 * @param buffer Buffer to be written into the file
		 */
		@SuppressWarnings("unused")
		public static void writeData(String outputFile, byte[] buffer) {
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(outputFile);
				fos.write(buffer);
				fos.close();
			} 
			catch (IOException e) {
				System.out.println("Error writing file " + outputFile + ": " + e.getMessage());
			}			
		}
	}

	private class DownloadUpdateStatusCallback implements IDownloadUpdateStatusCallback{

		public DownloadUpdateStatusCallbackReturnType downloadUpdateStatusCallbackMethod(DownloadUpdateStage stage, 
				long currentSize, long totalSize, IDownloadUpdateStatusUserdata userData)
		{
			/* Do whatever you want here to display progress */
			
			long percentComplete = 0;
			
			/* Display change in state */
			if (stage != ((DownloadUpdateStatusUserdata)userData).lastUpdateStage)
			{
				((DownloadUpdateStatusUserdata)userData).lastUpdateStage = stage;
				if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_DOWNLOAD_START) System.out.println("Download started.");
				else if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_DOWNLOAD_END) 
				{
					System.out.println();
					System.out.println("Download ended.");
				}
				else if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_EXECUTION_START) System.out.println("Execution started.");
				else if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_EXECUTION_END) System.out.println("Execution ended.");
			}
			
			if (stage == DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_DOWNLOADING)
			{
				if (totalSize > 0)
				{
					percentComplete = (long)(((double)((double)currentSize / (double)totalSize)) * 100);
					if (percentComplete > ((DownloadUpdateStatusUserdata)userData).lastPercentComplete)
					{
						int 					progressBarWidth = 50;
						int 					progressBarPos = 0;	
						
						progressBarPos = (int)(progressBarWidth * ((double)currentSize / (double)totalSize));
						System.out.print("\r");
						System.out.print("[");
						for (int i = 0; i < progressBarWidth; ++i) 
						{
							if (i < progressBarPos) System.out.print("=");
							else if (i == progressBarPos) System.out.print(">");
							else System.out.print(" ");
						}
						if (percentComplete >= 100) System.out.print("]");
						System.out.print(" " + percentComplete + " %");
						
						((DownloadUpdateStatusUserdata)userData).lastPercentComplete = percentComplete;
					}
				}
				else
				{
					System.out.print("\r");
					System.out.print("Downloading.  Please wait...");
				}
			}
			
			DownloadUpdateStatusCallbackReturnType	callbackReturnValue = DownloadUpdateStatusCallbackReturnType.FLC_UPDATE_STATUS_RETURN_CONTINUE;
			
			if ((userDefinedPauseAtPercentage > 0) && (percentComplete >= userDefinedPauseAtPercentage))
			{
				/* Simulating a simple scenario to test 'Pause' */
				System.out.println ("Download paused at " + userDefinedPauseAtPercentage + " percent.");
				callbackReturnValue = DownloadUpdateStatusCallbackReturnType.FLC_UPDATE_STATUS_RETURN_PAUSE;
				userDefinedPauseAtPercentage = 0;
			}
			else if ((totalSize > 0) && (currentSize > totalSize))
			{
				/* Something's odd.  Cancel the download. */
				callbackReturnValue = DownloadUpdateStatusCallbackReturnType.FLC_UPDATE_STATUS_RETURN_CANCEL;
			}
			
			return callbackReturnValue;
		}
	}
	
	private class DownloadUpdateStatusUserdata implements IDownloadUpdateStatusUserdata{

		/* Customize the user data object you'd like passed through the Update Status callback
		 * mechanism to suit your needs. */
		
		public long lastPercentComplete;
		public DownloadUpdateStage lastUpdateStage;

		public DownloadUpdateStatusUserdata()
		{
			lastPercentComplete = 0;
			lastUpdateStage = DownloadUpdateStage.DOWNLOAD_UPDATE_STAGE_IDLE;
		}	
	}
	
	private static void printException (FlxException e)
	{
		System.out.println("ERROR!");
		System.out.println(e.getMessage());
		
		Object exceptionArgs[] = new Object[e.getArguments().length];
        exceptionArgs = e.getArguments();
        for (Object arg: exceptionArgs)
        {
            System.out.println(arg.toString());
        }
        
		System.out.println("Stacktrace - ");
		e.printStackTrace();
	}

}
